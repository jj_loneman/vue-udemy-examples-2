import axios from 'axios';

const instance = axios.create({
  baseURL: 'https://axios-vue-example.firebaseio.com'
});

instance.defaults.headers.common['SOMETHING'] = 'something';

export default instance;