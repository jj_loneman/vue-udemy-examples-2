# 13 - Improving your App with Filters and Mixins

## 13.1 - Creating a Local Filter

* A filter is a syntax feature you can use in your template to transform output in the template
  * It doesn't transform the data itself, it only transforms what the users sees
  * An example is displaying a string in all CAPS
* Vue doesn't ship with filters, you have to create your own

`src/App.vue`

```html
<template>
  <div class="container">
    <div class="row">
      <div class="col-xs-12 col-sm-8 col-sm-offset-2 col-md-6 col-md-offset-3">
        <h1>Filters & Mixins</h1>
          <p>{{ text | toUpperCase}}</p>
      </div>
    </div>
  </div>
</template>

<script>
  export default {
    data() {
      return {
        text: 'Hello there!'
      };
    },
    filters: {
      toUppercase(value) {
        return value.toUpperCase();
      }
    }
  }
</script>
```

* This creates a local filter for the component `toUpperCase()`, which is a function
* The pipe in `<p>{{ text | toUpperCase }}</p>` is similar to Angular where it applies the filter to the property
* **Only transforms the value, not the actual data property!**
* Output:
  * ![](images/2017-12-05-00-22-23.png)

## 13.2 - Global Fitlers and How to Chain Multiple Filters

`src/main.js`

```javascript
import Vue from 'vue'
import App from './App.vue'

Vue.filter('to-lowercase', () => {
  return value.toLowerCase();
});

new Vue({
  el: '#app',
  render: h => h(App)
});
```

`src/App.vue`

```html
          <p>{{ text | toUpperCase | to-lowercase }}</p>
```

* This chains filters (output will be all lowercase)

## 13.3 - An Often-Times Better Alternative to Filters: Computed Properties

* Problem with filters:
  * If we had the code `<li v-for="fruit in fruits | filterFruits">{{ fruit }}</li>`, where `filterFruits()` filtered the fruits by us typing in the fruit name:
    * It's suboptimal from a performance standpoint
    * Vue isn't able to handle it very well in the background as it can't detect when it should re-render the filter or not
    * It'll re-render the filter in each re-rendering of the DOM
    * So even if you didn't input any text, it'll re-run the filter because it doesn't know that
  * Filters have been changed since Vue 1.0
* Computed properties
  * Vue only changes stuff in the `computed` property when it needs to

`src/App.vue`

```html
<template>
  <div class="container">
    <div class="row">
      <div class="col-xs-12 col-sm-8 col-sm-offset-2 col-md-6 col-md-offset-3">
        <h1>Filters & Mixins</h1>
        <p>{{ text | toUpperCase | to-lowercase }}</p>
        <hr>
        <input v-model="filterText">
        <ul>
          <li v-for="fruit in filteredFruits">{{ fruit }}</li>
        </ul>
      </div>
    </div>
  </div>
</template>

<script>
  export default {
    data() {
      return {
        text: 'Hello there!',
        fruits: ['Apple', 'Banana', 'Mango', 'Melon'],
        filterText: ''
      };
    },
    filters: {
      toUppercase(value) {
        return value.toUpperCase();
      }
    },
    computed: {
      filteredFruits() {
        return this.fruits.filter((element) => {
          return element.match(this.filterText);
        });
      }
    }
  }
</script>
```

* `filteredFruits()` returns an array of the elements that matches the user's input (bound to `filterText`)
* Output:
  * ![](images/2017-12-05-00-37-06.png)

## 13.4 - Creating and Using Mixins

* If we wanted to create a component `List.vue`, we'd copy most of the stuff in the `export default` in `App.vue`
* To avoid code duplication, we can use mixins

`src/fruitMixin.js`

```javascript
export default fruitMixin = {
  data() {
    return {
      fruits: ['Apple', 'Banana', 'Mango', 'Melon'],
      filterText: ''
    };
  },
  computed: {
    filteredFruits() {
      return this.fruits.filter((element) => {
        return element.match(this.filterText);
      });
    }
  }
};
```

* To use the mixin in another file, use it like so:

```html
<script>
  import { fruitMixin } from './fruitMixin';

  export default {
    mixins: [fruitMixin]
  }
</script>
```

* These are our new files:

`src/App.vue`

```html
<template>
  <div class="container">
    <div class="row">
      <div class="col-xs-12 col-sm-8 col-sm-offset-2 col-md-6 col-md-offset-3">
        <h1>Filters & Mixins</h1>
        <p>{{ text | toUpperCase | to-lowercase }}</p>
        <hr>
        <input v-model="filterText">
        <ul>
          <li v-for="fruit in filteredFruits">{{ fruit }}</li>
        </ul>
        <hr>
        <app-list></app-list>
      </div>
    </div>
  </div>
</template>

<script>
  import List from './List';
  import { fruitMixin } from './fruitMixin';

  export default {
    mixins: [fruitMixin],
    data() {
      return {
        text: 'Hello there!',
      };
    },
    filters: {
      toUppercase(value) {
        return value.toUpperCase();
      }
    },
    components: {
      appList: List
    }
  }
</script>
```

`src/List.vue`

```html
<template>
  <div>
    <h1>Filters & Mixins</h1>
    <hr>
    <input v-model="filterText">
    <ul>
      <li v-for="fruit in filteredFruits">{{ fruit }}</li>
    </ul>
  </div>
</template>

<script>
  import { fruitMixin } from './fruitMixin';

  export default {
    mixins: [fruitMixin]
  }
</script>
```

* Output:
  * ![](images/2017-12-05-00-47-42.png)

## 13.5 - How Mixins get Merged

* In our `App.vue` case, we load the `data` property from the mixin as well as from the component itself - it merges the two together
* In the merging process, it tries to add the new things in the mixin to the existing instance
* In some things, like lifecyle hooks, the behavior that we are able to provide: a lifecycle hook and a mixin, and in our component or instance, both will get executed, even if it's the same name
  * Then the order is 1. mixin, 2. component

`src/fruitMixin.js`

```javascript
...
  created() {
    console.log('Created');
  }
...
```

* If we open the console, when we load the app, we see `Created` twice b/c we use the mixin twice:
  * ![](images/2017-12-05-00-56-09.png)
  * So both `App` and `List` execute the `created()` lifecycle hook since the mixin has it

`src/List.vue`

```html
<script>
  import { fruitMixin } from './fruitMixin';

  export default {
    mixins: [fruitMixin],
    created() {
      console.log('Inside List Created Hook');
    }
  }
</script>
```

* If we open the console, we see this:
  * ![](images/2017-12-05-00-57-27.png)
  * The order is mixin first, then the component
  * This is because the component has the ability to override any changes by the mixin
    * B/c of this, the mixin can't destroy anything

## 13.6 - Creating a Global Mixin (Special Case)

* A global mixin is added to every instance (and thus every component) in the app
* **You rarely want to use them**
  * You really only use them if you are creating third-party plugins for Vue

`src/main.js`

```javascript
import Vue from 'vue'
import App from './App.vue'

Vue.filter('to-lowercase', () => {
  return value.toLowerCase();
});

Vue.mixin({
  created() {
    console.log('Global mixin - created hook');
  }
});

new Vue({
  el: '#app',
  render: h => h(App)
});
```

* Here is how you register a mixin globally
* Output when run the app:
  * ![](images/2017-12-05-01-01-37.png)

## 13.7 - Mixins and Scope

`src/App.vue`

```html
        <button @click="fruits.push('Berries')">Add New Item</button>
```

* If we add this code, when we run the app, it only gets added to the first list in the `App` component:
  * ![](images/2017-12-05-01-05-53.png)
* **The `fruitMixin` isn't shared, it's replicated**
  * So you can share the object and manipulate data, but only affects that component
* If you want to manipulate shared data, use event bus