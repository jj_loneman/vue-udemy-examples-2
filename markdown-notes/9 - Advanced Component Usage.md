# 9 - Advanced Component Usage

## 9.1 - Setting Up Project

`src/App.vue`

```html
<template>
    <div class="container">
        <div class="row">
            <div class="col-xs-12">
               <app-quote></app-quote>
            </div>
        </div>
    </div>
</template>

<script>
    import Quote from './components/Quote';

    export default {
        components: {
          appQuote: Quote
        }
    }
</script>

<style>
</style>
```

`src/components/Quote.vue`

```html
<template>
  <div>
    <p>A wonderful Quote!</p>
  </div>
</template>

<script>
  export default {
    name: "quote"
  }
</script>

<style scoped>
  div {
    border: 1px solid #ccc;
    box-shadow: 1px 1px 2px black;
    padding: 30px;
    margin: 30px auto;
    text-align: center;
  }
</style>
```

Output:
![](images/2017-12-03-20-41-35.png)

## 9.2 - Passing Content - The Suboptimal Solution

`src/components/Quote.vue`

```html
<template>
  <div>
    <p>{{ quote }}</p>
  </div>
</template>

<script>
  export default {
    props: ['quote']
  }
</script>

<style scoped>
  div {
    border: 1px solid #ccc;
    box-shadow: 1px 1px 2px black;
    padding: 30px;
    margin: 30px auto;
    text-align: center;
  }
</style>
```

* Added string interpolation into `<p>` tag with `{{ quote }}` and exported `quote` in props
* Output is still the same
* Our goal is to pass information like this in the `App` component:

`src/App.vue`

```html
      <div class="col-xs-12">
        <app-quote>
          <h2>The Quote</h2>
          <p>A wonderful quote</p>
        </app-quote>
      </div>
```

* But that won't work - we can fix this using slots

## 9.3 - Passing Content with Slots

`src/App.vue`

```html
<template>
  <div class="container">
    <div class="row">
      <div class="col-xs-12">
        <app-quote>
          <h2>The Quote</h2>
          <p>A wonderful quote</p>
        </app-quote>
      </div>
    </div>
  </div>
</template>
```

`src/components/Quote.vue`

```html
<template>
  <div>
    <slot></slot>
  </div>
</template>
```

* `<slot>` is a reserved Vue element
* It takes over and renders the content in the `<app-quote>` tag in `App.vue`
* Slots allows us to pass data from outside and render inside the component

Output:
![](images/2017-12-03-21-15-16.png)

## 9.4 - How Slot Content gets Compiled and Styled

`src/components/Quote.vue`

```html
<style scoped>
  ...
    h2 {
    color: red;
  }
  ...
```

* Output:
  * ![](images/2017-12-03-21-19-19.png)
* The styling of the `Quote` component (the `h2` styling) is applied in `Quote.vue`
* Child data styling is applied to the data component passed in from outside
* If we put the same `h2` styling into `App.vue` instead, it wouldn't work

`src/components/App.vue`

```html
        <app-quote>
          <h2>{{ quoteTitle }}</h2>
          <p>A wonderful quote</p>
        </app-quote>
...
<script>
  import Quote from './components/Quote';

  export default {
    data: function() {
      return {
        quoteTitle: 'The Quote'
      }
    },
...
```

* The code still works the same
* For compiling the template, which means rendering any kind of Vue application, the component where you have the code in the template will be the one doing the changes
* In this case, the `App` component (the `App` root instance of Vue) is the one doing the changes
  * Even though we pass it to the child component, it is in the `App` component's template where we use the `quoteTitle` in the first place
* **The styling is set up in the child component**
* **Everything else is handled in the parent component**

## 9.5 - Using Multiple Slots (Named Slots)

`src/components/Quote.vue`

```html
<template>
  <div>
    <div class="title">
      <slot name="title"></slot>
    </div>
    <hr>
    <div>
      <slot name="content"></slot>
    </div>
  </div>
</template>
...
<style>
  ...
    .title {
    font-style: italic;
  }
</style>
```

`src/App.vue`

```html
<template>
  <div class="container">
    <div class="row">
      <div class="col-xs-12">
        <app-quote>
          <h2 slot="title">{{ quoteTitle }}</h2>
          <p slot="content">A wonderful quote</p>
        </app-quote>
      </div>
    </div>
  </div>
</template>
```

* By naming the slots, it'll render the different parts of the component into those slots
* Output:
  * ![](images/2017-12-03-21-51-30.png)

## 9.6 - Default Slots and Slot Defaults

`src/App.vue`

```html
        <app-quote>
          <h2 slot="title">{{ quoteTitle }}</h2>
          <p>A wonderful quote</p>
        </app-quote>
```

`src/components/Quote.vue`

```html
<template>
  <div>
    <div class="title">
      <slot name="title"></slot>
    </div>
    <hr>
    <div>
      <slot></slot>
    </div>
  </div>
</template>
```

* If we remove the named `content` slot for the second one, it's treated as the "default" slot
* So in `App.vue`, since the `<p>` tag isn't assigned a slot, it'll automatically get rendered in the default slot
* The output is the same as before in this case

`src/components/Quote.vue`

```html
<template>
  <div>
    <div class="title">
      <slot name="title"></slot>
      <span style="color: #ccc">
        <slot name="subtitle">The Subtitle</slot>
      </span>
    </div>
    <hr>
    <div>
      <slot></slot>
    </div>
  </div>
</template>
```

* In this case, we added a slot named `subtitle` in the `Quote` component even though we don't assign something to it from the `App` component
* Output:
  * ![](images/2017-12-03-22-06-30.png)
* Whenever we do insert content into the `subtitle` slot, the default content in there will get replaced
  * But in this case, it will be displayed when we don't have data passed in from the outside (whenever the slot is unoccupied)

## 9.7 - Slots Summary

* Slots help you to distribute content in other components
* Especially useful when you want to build reusable widgets like a slideshow where you have the frame for each slide and you want to switch between slides
  * Each slide might have its own HTML layout
  * But you want to use the same frame
  * You can use slots to make sure the content you want to pass into the frame (the HTML layout) is distributed exactly the way you want it distributed inside of the frame and component

## 9.8 - Switching Multiple Component with Dynamic Components

`src/App.vue`

```html
<template>
  <div class="container">
    <div class="row">
      <div class="col-xs-12">
        <button @click="selectedComponent = 'appQuote'">Quote</button>
        <button @click="selectedComponent = 'appAuthor'">Quote</button>
        <button @click="selectedComponent = 'appNew'">Quote</button>
        <hr>
        <p>{{ selectedComponent }}</p>
        <button>Author</button>
        <button>New</button>
        <hr>
        <app-quote>
          <h2 slot="title">{{ quoteTitle }}</h2>
          <p>A wonderful quote</p>
        </app-quote>
      </div>
    </div>
  </div>
</template>

<script>
  import Quote from './components/Quote';
  import Author from './components/Author'
  import New from './components/New'

  export default {
    data: function() {
      return {
        quoteTitle: 'The Quote',
        selectedComponent: 'appQuote'
      }
    },
    components: {
      appQuote: Quote,
      appAuthor: Author,
      appNew: New,
    }
  }
</script>

<style>
</style>
```

* The name `appQuote` should be the same as the component name
  * ![](images/2017-12-03-22-24-31.png)
* Output:
  * ![](images/2017-12-03-22-31-44.png)
  * Clicking the buttons changes the `selectedComponent`
  * But we want to actually change loaded component

### New Code

`src/App.vue`

```html
<template>
  <div class="container">
    <div class="row">
      <div class="col-xs-12">
        <button @click="selectedComponent = 'appQuote'">Quote</button>
        <button @click="selectedComponent = 'appAuthor'">Quote</button>
        <button @click="selectedComponent = 'appNew'">Quote</button>
        <hr>
        <p>{{ selectedComponent }}</p>
        <button>Author</button>
        <button>New</button>
        <hr>
        <component :is="selectedComponent">
          <p>Default Content</p>
        </component>
        <!--<app-quote>-->
          <!--<h2 slot="title">{{ quoteTitle }}</h2>-->
          <!--<p>A wonderful quote</p>-->
        <!--</app-quote>-->
      </div>
    </div>
  </div>
</template>
```

* `<component>` is a reserved name
  * Allows us to dynamically bind components
  * Use `:is` to bind the `is` attribute to a dynamic data property
* This component gets bound to the `selectedComponent` defined in the `data` function
  * Whenever the string of `selectedComponent` is changed, it'll change the bound component
* Output of clicking "Quote" button:
  * ![](images/2017-12-03-22-40-48.png)
  * Default component
* Output of clicking "Author" button
  * ![](images/2017-12-03-22-41-13.png)
* Output of clicking "New" button
  * ![](images/2017-12-03-22-41-30.png)

## 9.9 - Understanding Dynamic Component Behavior

`src/components/New.vue`

```html
<template>
  <div>
    <h3>New Quote</h3>
    <button @click="counter++">Increase!</button>
    <p>{{ counter }}</p>
  </div>
</template>

<script>
  export default {
    data: function() {
      return {
        counter: 0
      };
    },
    destroyed() {
      console.log('Destroyed!');
    }
  }
</script>
```

* Here we increase a counter for the `New` component:
  * ![](images/2017-12-03-22-45-49.png)
* When we switch to another component, `Author`:
  * ![](images/2017-12-03-22-46-22.png)
  * The `New` component gets destroyed
  * When you go back to `New`, `counter` will reset to `0`

## 9.10 - Keeping Dynamic Components Alive

`src/App.vue`

```html
        <keep-alive>
          <component :is="selectedComponent">
            <p>Default Content</p>
          </component>
        </keep-alive>
```

* `<keep-alive>` makes sure that the component is kept alive
* Now, when we incrase the counter in the `New` component, switch to another component, and go back to the `New` component, it's kept alive:
  * ![](images/2017-12-04-18-45-12.png)

## 9.11 - Dynamic Component Lifecycle Hooks

* Lose the `destroyed()` lifecycle when component is kept alive
* `deactivated()` - executed when on the component and load another one
* `activated()` - executed when do load the dynamic component
  * Output when going to `New` component:
    * ![](images/2017-12-04-18-49-19.png)
  * Output when clicking away from the `New` component:
  * ![](images/2017-12-04-18-49-43.png)