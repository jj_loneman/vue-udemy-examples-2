# 14 - Adding Animations and Transitions

## 14.1 - Understanding Transitions

* Animations are helpful to the user to let them know where they are or how to use your app
* You can animate adding an element to the DOM:
  * ![](images/2017-12-05-01-16-16.png)
* And also animate removing an element from the DOM:
  * ![](images/2017-12-05-01-16-36.png)
* You can do that with the `<transition>` wrapper element

## 14.2 - Preparing Code to use Transitions

`src/App.vue`

```html
        <transition>
          <div class="alert" v-if="show">This is some info</div>
        </transition>
```

* `<transition>` is a special component which animates anything inside it
* **You can only animate one thing inside the `<transition>`**
  * The code will break if you try to animate more than one thing
* This code alone doesn't do an animation

## 14.3 - Setting Up a Transition

* **The transition only works if the element already exists in your code and is toggled with `v-if` or `v-show` for example**
  * Doesn't work if you add the element later

![](images/2017-12-05-01-31-32.png)

* First attaches a css class (which you can name) called `whatever-enter`
  * This is the initial element
  * Might call it `fade-enter`
    * You'd set the opacity to 0 to be initially transparent
  * Only applies it for one frame at the beginning of the animation duration
* Next, attaches `*-enter-active` class
  * This will be applied until the animation finishes
  * `fade-enter-active`
  * The length of the animation is determined by Vue automatically by the length of the transitions or animations you have set up in your code
* Once the element is in the DOM, the previous two classes are removed
* When the element leaves, attaches class `*-leave`
  * `fade-leave`
  * Applied for one frame
* Then, attaches `*-leave-active` class
  * Applied until the animation finishes
* If you don't use a name, the default css name is called `v-enter`, etc.

## 14.4 - Assigning CSS Classes for Transitions

`src/App.vue`

```html
<template>
...
        <transition name="fade">
          <div class="alert" v-if="show">This is some info</div>
        </transition>
...
</template>
...
<style>

</style>
```

* In `<transition>`, we set `name="fade"` not `:name` since we don't want to bind `name` to a property, we want to hardcode the css class name in
  * It'll attached `fade` to all the css class names, like `fade-enter`, etc.

## 14.5 - Creating a "Fade" Transition with the CSS Transition Property

`src/App.vue`

```html
<style>
  .fade-enter {
    opacity: 0;
  }

  .fade-enter-active {
    transition: opacity 1s;
  }

  .fade-leave {
  }

  .fade-leave-active {
    transition: opacity 1s;
    opacity: 0;
  }
</style>
```

* Don't need to specify `opacity: 1` in `.fade-enter-active` and `.fade-leave` because that is the default

## 14.6 - Creating a "Slide" Transition with the CSS Animation Property

`src/App.vue`

```html
<template>
...
        <transition name="slide">
          <div class="alert" v-if="show">This is some info</div>
        </transition>
...
</template>
<style>
...
  .slide-enter {

  }

  .slide-enter-active {
    animation: slide-in 1s ease-out forwards;
  }

  .slide-leave {

  }

  .slide-leave-active {
    animation: slide-out 1s ease-out forwards;
  }

  @keyframes slide-in {
    from {
      transform: translateY(20px);
    }
    to {
      transform: translateY(0);
    }
  }

  @keyframes slide-out {
    from {
      transform: translateY(0);
    }
    to {
      transform: translateY(20px);
    }
  }
</style>
```

* In `.slide-enter`, you could insert `transform: translateY(20px)` since that is the beginning state, but since it's set up as a keyframe, don't need it there
* In `.slide-enter-active`:
  * `ease-out` means end a bit slower than we start
  * `forwards` means the element stays in the finishing position of the animation and doesn't snap back to the start

## 14.7 - Mixing Transition and Animation Properties

`src/App.vue`

```html
<style>
...
  .slide-enter-active {
    animation: slide-in 1s ease-out forwards;
    transition: opacity .5s;
  }

  .slide-leave {

  }

  .slide-leave-active {
    animation: slide-out 1s ease-out forwards;
    transition: opacity 3s;
    opacity: 0;
  }
...
</style>
```

* Start of animation:
  * ![](images/2017-12-05-01-49-13.png)
* End of animation:
  * ![](images/2017-12-05-01-49-29.png)
* The second element weirdly jumps up when finishing the animation
  * This is because we have both `animation` and `transition` in our CSS
    * It looks ugly if it uses the transition
    * Vue doesn't know which one to use
    * It takes the longer one
* We can tell Vue which property to use in the `type` attribute of `<transition>`
  * `<transition name="slide" type="animation">`
    * `type` can be `animation` or `transition`
    * With `animation`, it will make sure the animation finishes and not the transition finishes before it removes the element

## 14.8 - Animating v-if and v-show

* Instead of `v-if`, you can use `v-show`, which doesn't actually remove the element, it just triggers the display

## 14.9 - Setting Up an Initial (on-load) Animation

* The `appear` attribute will make it animate on page load
* `<transition name="fade" appear>`

## 14.10 - Using Different CSS Class Names

* `Animate.css`: <https://daneden.github.io/animate.css/>
* If you don't want to use default names, you can set your own like so:

`src/App.vue`

```html
        <transition
            enter-active-class="animated bounce"
            leave-active-class="animated shake"
        >
```

* You could also override `enter-class` and `leave-class`
* **Note: `appear` doesn't work with custom classes, only default ones**

## 14.11 - Using Dynamic Names and Attributes

`src/App.vue`

```html
<template>
...
        <select v-model="alertAnimation" class="form-control">
          <option value="fade">Fade</option>
          <option value="slide">Slide</option>
        </select>
        <button class="btn btn-primary" @click="show = !show">Show Alert</button>
        <br><br>
        <transition :name="alertAnimation">
          <div class="alert" v-if="show">This is some info</div>
        </transition>
...
</template>
<script>
  export default {
    data() {
      return {
        show: true,
        alertAnimation: 'fade'
      }
    }
  }
</script>
```

* Output:
  * ![](images/2017-12-05-02-08-30.png)
* Bound `alertAnimation` here and used `:name="alertAnimation"`

## 14.12 - Transitioning between Multiple Elements

* An example is once an element starts leaving, you want to start animating another one
* `key`
  * When you use the same element twice, Vue can't differentiate b/t the two
    * An example of this is the `alert-info` / `alert-warning` div
  * For Vue, it's the same - it can only swap the content and not the elements themselves
  * The `key` Vue attribute gives the element a unique name
* `mode`
  * Because of the default behavior that `transition` has, the transition between the `info` and the `warning` isn't clean since they're posititioned underneath other element
    * If they had `position: absolute` where they were on top of each other, then you'd probably get the desired effect
  * `out-in`: let the old element animate out first, then animate the new one in
  * `in-out`: opposite

`src/App.vue`

```html
        <transition :name="alertAnimation" mode="out-in">
          <div class="alert alert-info" v-if="show" key="info">This is some info</div>
          <div class="alert alert-warning" v-else key="warning">This is some warning</div>
        </transition>
```

## 14.13 - Listening to Transition JS Hooks

![](images/2017-12-05-02-24-12.png)

* `before`
* `enter`
  * This is where you'd typically play the animation
* `after-enter`
  * Fires when animation is finished
  * Note: you can combine all of these with your css classes
* `after-enter-cancelled`
  * Occurs whenever we change the condition which would add the element before the animation finished
* `before-leave`
* `leave`
* `after-leave`
* `after-leave-cancelled`

## 14.14 - Understanding JavaScript Animations

`src/App.vue`

```html
<template>
...
        <transition
          @before-enter="beforeEnter"
          @enter="enter"
          @after-enter="afterEnter"
          @enter-cancelled="enterCancelled"

          @before-leave="beforeLeave"
          @leave="leave"
          @after-leave="after-leave"
          @leave-cancelled="leaveCancelled"
        >
          <div
              style="width: 100px; height: 100px; background-color: lightgreen"
              v-if="load"
          ></div>
        </transition>
...
</template>
<script>
  export default {
    data() {
      return {
        show: false,
        load: true,
        alertAnimation: 'fade'
      }
    },
    methods: {
      beforeEnter(el) {
        console.log('beforeEnter');
      },
      enter(el, done) {
        console.log('enter');
        done();
      },
      afterEnter(el) {
        console.log('afterEnter');
      },
      enterCancelled(el) {
        console.log('enterCancelled');
      },
      beforeLeave(el) {
        console.log('beforeLeave');
      },
      leave(el, done) {
        console.log('leave');
        done();
      },
      afterLeave(el) {
        console.log('afterLeave');
      },
      leaveCancelled(el) {
        console.log('leaveCancelled');
      }
    }
  }
</script>
```

* `done()` is needed to tell Vue once the animation finishes
  * When using css animations, it can determine it from the timing defined in it
  * You don't need to call `done()` when you use css animations
* Output
  * Initial page load
    * ![](images/2017-12-05-02-34-28.png)
  * Removing the element
    * ![](images/2017-12-05-02-34-59.png)
  * Adding it again
    * ![](images/2017-12-05-02-35-16.png)

## 14.15 - Excluding CSS from your Animation

`src/App.vue`

```html
        <transition
          ...
          :css="false"
        >
```

* Not using any CSS here, but if we directly tell Vue if we're using CSS or not, it can skip the step of trying to determine it itself
* If you don't have `name="myCssClass"` set up, it doesn't mean it doesn't try to look for css classes
  * It'll try to look for `v-enter`, etc. and see that those classes don't exist
* You can skip the check with `:css="false"`
  * Use the colon `:css` instead of just `css` since you want to pass `false` as a boolean instead of a string

## 14.16 - Creating an Animation in Javascript

* The places to animate are the `enter` and `leave` functions since they execute after the initial states are done

`src/App.vue`

```html
<script>
  export default {
    data() {
      return {
        show: false,
        load: true,
        alertAnimation: 'fade',
        elementWidth: 100
      }
    },
    methods: {
      beforeEnter(el) {
        console.log('beforeEnter');
        this.elementWidth = 100;
        el.style.width = this.elementWidth + 'px';
      },
      enter(el, done) {
        console.log('enter');
        let round = 1;
        const interval = setInterval(() => {
          el.style.width = (this.elementWidth + round * 10) + 'px';
          round++;
          if (round > 20) {
            clearInterval(interval);
            done();
          }
        }, 20);
      },
      afterEnter(el) {
        console.log('afterEnter');
      },
      enterCancelled(el) {
        console.log('enterCancelled');
      },
      beforeLeave(el) {
        console.log('beforeLeave');
        this.elementWidth = 300;
        el.style.width = this.elementWidth + 'px';
      },
      leave(el, done) {
        console.log('leave');
        let round = 1;
        const interval = setInterval(() => {
          el.style.width = (this.elementWidth - round * 10) + 'px';
          round++;
          if (round > 20) {
            clearInterval(interval);
            done();
          }
        }, 20);
        done();
      },
      afterLeave(el) {
        console.log('afterLeave');
      },
      leaveCancelled(el) {
        console.log('leaveCancelled');
      }
    }
  }
</script>
```

* Important to set `this.elementWidth` in the `beforeEnter` and `beforeLeave` methods since the element might not be in the state you want when the animation starts
* Output:
  * When load the element, it grows to 300px
    * ![](images/2017-12-05-02-48-44.png)
  * When remove the element, it shrinks
    * ![](images/2017-12-05-02-49-18.png)

## 14.17 - Adding Dynamic Components

`src/DangerAlert.vue`

```html
<template>
  <div class="alert alert-danger">This is dangerous!</div>
</template>
```

`src/SuccessAlert.vue`

```html
<template>
  <div class="alert alert-success">This is success!</div>
</template>
```

`src/App.vue`

```html
<template>
...
        <button
            class="btn btn-primary"
            @click="selectedComponent === 'app-success-alert' ? selectedComponent = 'app-danger-alert' : selectedComponent = 'app-success-alert'"
        >
          Toggle Components
        </button>
        <br><br>
        <transition name="fade" mode="out-in">
          <component :is="selectedComponent"></component>
        </transition>
...
</template>
<script>
  import DangerAlert from './DangerAlert';
  import SuccessAlert from './SuccessAlert';

  export default {
    data() {
      return {
        show: false,
        load: true,
        alertAnimation: 'fade',
        elementWidth: 100,
        selectedComponent: 'app-success-alert'
      }
    },
    ...
    components: {
      appDangerAlert: DangerAlert,
      appSuccessAlert: SuccessAlert,
    }
  }
</script>
```

* Pressing the toggle button alternates between success and alert:
  * ![](images/2017-12-05-02-58-43.png)
  * ![](images/2017-12-05-02-59-01.png)

## 14.18 - Animating Lists with \<transition-group\>

`src/App.vue`

```html
<template>
...
        <button
            class="btn btn-primary"
            @click="addItem"
        >
          Add Item
        </button>
        <br><br>
        <ul class="list-group">
          <li
              class="list-group-item"
              v-for="(number, index) in numbers"
              @click="removeItem(index)"
              style="cursor: pointer"
          >
            {{ number }}
          </li>
        </ul>
...
</template>
<script>
...
    data() {
      return {
        ...
        numbers: [1, 2, 3, 4, 5]
      };
    },
    methods: {
      ...
      addItem() {
        const pos = Math.floor(Math.random * this.numbers.length);
        this.numbers.splice(pos, 0, this.numbers.length + 1);
      },
      removeItem(index) {
        this.numbers.splice(index, 1);
      }
    },
    ...
</script>
```

* Output:
  * ![](images/2017-12-05-03-09-28.png)
* Clicking on a list item removes it from the list
* `splice()` modifies an array
  `splice(index, amount_to_remove, index_to_add_at)`

## 14.19 - Using \<transition-group\> to Animate a List

`src/App.vue`

```html
<template>
...
        <ul class="list-group">
          <transition-group name="slide">
            <li
                class="list-group-item"
                v-for="(number, index) in numbers"
                @click="removeItem(index)"
                style="cursor: pointer"
                :key="number"
            >
              {{ number }}
            </li>
          </transition-group>
        </ul>
...
</template>
...
<style>
...
  .slide-leave-active {
    animation: slide-out 1s ease-out forwards;
    transition: opacity 1s;
    opacity: 0;
    position: absolute;
  }

  .slide-move {
    transition: transform 1s;
  }
...
</style>
```

* `<transition>` is not rendered in the DOM
* `<transition-group>` does render in a new HTML tag
  * By default, this will be a `<span>`
  * You can overwrite this by setting `<transition-group tag="TAG">`
* In the code, `<li>` will be replicated, so you need `<transition-group>`
* If we didn't have `key` in the `<li>` element, the list wouldn't display at all
  * Otherwise, Vue can't identify individual items in the list
  * It needs to because when it animates them, it needs to move the existing elements
  * It needs to know which one is the right one to move
* Using `<transition-group>` also gives us access to the `.slide-move` css class
  * Attached to any element that needs to change its place
* `transition: transform 1s`
  * Vue always uses `transform` or `translateX()` or `translateY()`, whatever is appropriate to make elements move their position behind the scenes
  * Whenever `transform` is called, animate it over `1s`
* In `.slide-leave-active`, we add a `position: absolute` so that when we remove an item, it doesn't jump
* Output:
  * ![](images/2017-12-05-03-23-01.png)