# 11 - Handling User Input with Forms

## 11.1 - A Basic \<input\> Form Binding

`src/App.vue`

```html
...
            <input
                type="text"
                id="email"
                class="form-control"
                v-model="email"
            >
            ...
            <p>Mail: {{ email }}</p>
            <p>Password:</p>
            <p>Age:</p>
            <p>Message: </p>
...
<script>
  export default {
    data() {
      return {
        email: ''
      }
    }
  }
</script>
```

* Vue will figure out which direction the binding needs to be
  * For example, if you type in something in the `email` filed, it'll update the property with that data
  * If you update a property from within the code, it'll update that component

## 11.2 - Grouping Data and Pre-Populating Inputs

`src/App.vue`

```html
<template>
...
          <div class="form-group">
            <label for="email">Mail</label>
            <input
                type="text"
                id="email"
                class="form-control"
                v-model="userData.email"
            >
          </div>
          <div class="form-group">
            <label for="password">Password</label>
            <input
                type="password"
                id="password"
                class="form-control"
                v-model="userData.password"
            >
          </div>
          <div class="form-group">
            <label for="age">Age</label>
            <input
                type="number"
                id="age"
                class="form-control"
                v-model="userData.age"
            >
          </div>
            ...
            <p>Mail: {{ userData.email }}</p>
            <p>Password: {{ userData.password }}</p>
            <p>Age: {{ userData.age }}</p>
...
</template>

<script>
  export default {
    data() {
      return {
        userData: {
          email: '',
          password: '',
          age: 27
        }
      }
    }
  }
</script>
```

* You should group data in a common object `userData` in this example

## 11.3 - Modifying User Input with Input Modifiers

`src/App.vue`

```html
            <input
                type="password"
                id="password"
                class="form-control"
                v-model.lazy="userData.password"
            >
            <p>{{ userData.password }}</p>
```

* If you use `.lazy`, the `<p>` tag will only update after we've typed something into the password field and then changed focus to another element
  * Output:
    * ![](images/2017-12-04-20-46-45.png)
  * Otherwise, the default function is for Vue to automatically react to the input typed in immediately
* Other modifiers:
  * `trim` to trim excess whitespace
  * `number` forces that the input is converted to a number
* You can chain modifiers like `.lazy.number`

## 11.4 - Binding \<textarea\> and Saving Line Breaks

* **Note: String interpolation between `<textarea>{{ text }}</textarea>` doesn't work!**
* Use `v-model` instead

`src/App.vue`

```html
<template>
...
          <textarea
              id="message"
              rows="5"
              class="form-control"
              v-model="message"
          ></textarea>
...
          <p style="white-space: pre">Message: {{ message }}</p>
...
</template>
```

* By applying a `white-space: pre` style to the tag where we output the `message` (from the textarea), if we have data that contains linebreaks, the tag will now output text with linebreaks like so:
  * ![](images/2017-12-04-21-03-23.png)

## 11.5 - Using Checkboxes and Saving Data in Arrays

`src/App.vue`

```html
            <label for="sendmail">
              <input
                  type="checkbox"
                  id="sendmail"
                  value="SendMail"
                  v-model="sendMail"
              > Send Mail
            </label>
            <label for="sendInfomail">
              <input
                  type="checkbox"
                  id="sendInfomail"
                  value="SendInfoMail"
                  v-model="sendMail"
              > Send Infomail
            </label>
            ...
            <p><strong>Send Mail?</strong></p>
            <ul>
              <li v-for="item in sendMail">{{ item }}</li>
            </ul>
...
<script>
  ...
  sendMail: []
...
```

* Vue will automatically merge the values of both checkboxes into the `sendMail` array
* Output when checking both checkboxes:
  * ![](images/2017-12-04-21-12-34.png)

## 11.6 - Using Radio Buttons

`src/App.vue`

```html
          <label for="male">
            <input
                type="radio"
                id="male"
                value="Male"
                v-model="gender"
            > Male
          </label>
          <label for="female">
            <input
                type="radio"
                id="female"
                value="Female"
                v-model="gender"
            > Female
          </label>
```

* When you bind multiple radio buttons to the same property, Vue will automatically make the radio button group mutually exclusive:
  * ![](images/2017-12-04-21-16-00.png)

## 11.7 - Handling Dropdowns with \<select\> and \<option\>

`src/App.vue`

```html
          <select
              id="priority"
              class="form-control"
          >
            <option v-for="priority in priorities">{{ priority }}</option>
          </select>
...
<script>
  ...
    priorities: ['High', 'Medium', 'Low']
...
```

* Output:
  * ![](images/2017-12-04-21-21-27.png)
* We can select an option by default like so:

`src/App.vue`

```html
            <option
                v-for="priority in priorities"
                :selected="priority === 'Medium'"
            >{{ priority }}
            </option>
```

* So when the form loads, it'll select 'Medium' by default
* `:selected` needs a `true` or `false` value

New Code:

`src/App.vue`

```html
          <select
              id="priority"
              class="form-control"
              v-model="selectedPriority"
          >
            <option
                v-for="priority in priorities"
                :selected="priority === 'Medium'"
            >{{ priority }}
            </option>
          </select>
...
<script>
...
    selectedPriority = 'High'
```

* Default priority is 'High':
  * ![](images/2017-12-04-21-28-25.png)
  * Even though we set the check written in the `:selected` attribute, it's overwritten by the model we're binding to (which is another way to set the default)

## 11.8 - What v-model does and How to Create a Custom Control

* `v-model="userData.email"`
* `v-model` does two things behind the scenes:
  * Binds the `value` like so:
    * `:value="userData.email"` (same as `v-bind:`)
    * It has to have a prop name `value` in order to be able to pass something into the component
  * And adds the `input` listener like so:
    * `@input="userData.email = $event.target.value"`
    * And it has to emit an event called `input` to allow `v-model` to react to that
* The code:
  ```html
  <input
    v-model="userData.email"
    ...
  ```
  is equivalent to:
  ```html
  <input
    :value=userData.email
    @input=userData.email = $event.target.value
    ...
  ```

## 11.9 - Creating a Custom Control (Input)

`src/Switch.vue`

```html
<template>
  <div>
    <div
        id="on"
        @click="switched(true)"
        :class="{active: isOn}"
    >
      On
    </div>
    <div
        id="off"
        @click="switched(false)"
        :class="{active: !isOn}"
    >
      Off
    </div>
  </div>
</template>

<script>
  export default {
    props: ['value'],
    methods: {
      switched(isOn) {
        this.$emit('input', isOn);
      }
    }
  }
</script>
```

* When the `<div>` is clicked on, the boolean value will be passed to the `switched()` method

`src/App.vue`

```html
        <div class="col-xs-12 col-sm-8 col-sm-offset-2 col-md-6 col-md-offset-3">
          <app-switch v-model="dataSwitch"></app-switch>
        </div>
        ...
        <p>Switched: {{ dataSwitch }}</p>
```

* Output:
  * ![](images/2017-12-04-22-11-36.png)
  * When you click `Off`, it outputs "false"

## 11.10 - Submitting a Form

`src/App.vue`

```html
          <button
              class="btn btn-primary"
              @click.prevent="submitted"
          >Submit!
          </button>
...
    <div class="row" v-if="isSubmitted">
      <div class="col-xs-12 col-sm-8 col-sm-offset-2 col-md-6 col-md-offset-3">
        <div class="panel panel-default">
          <div class="panel-heading">
            <h4>Your Data</h4>
...
<script>
...
    data() : {
      ...
        isSubmitted: false
      }
    },
    methods: {
      submitted() {
        this.isSubmitted = true;
      }
    },
```

* When you click a button, by default, the form will be sumitted to the server
  * This is not the action we want, so use `@click.prevent` to prevent that default action from occurring since we want Vue to handle it
* When you hit "Submit", you see the data div appear:
  * ![](images/2017-12-04-22-17-16.png)