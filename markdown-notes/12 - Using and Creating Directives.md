# 12 - Using and Creating Directives

## 12.1 - Understanding Directives

* `v-whatever` tells Vue it's not a normal HTML attribute, but a Vue directive

`src/App.vue`

```html
        <p v-text="'Some Text'"></p>
        <p v-html="'<strong>Some Strong Text</strong>'"></p>
```

* Output:
  * ![](images/2017-12-04-22-28-24.png)

## 12.2 - How Directives Work - Hook Functions

* A directive has five methods we can hook in to interact with the elements it sits on:
  
### bind()

* `bind(el, binding, vnode)`
* Fired as soon as the directive is bound to the element (as soon as we have access)
* `el`: Refers to the element the directive sits on
* `binding`: Refers to the way the directive is set up
  * Which arguments or modifiers passed to it, etc.
* `vnode`: Refers to the node in the virtual DOM (the virtual node)
* Both `binding` and `vnode` are read-only
  * You shouldn't change since they're set up by Vue at runtime

### inserted()

* `inserted(el, binding, vnode)`
* Occurs as soon as it is inserted in the DOM
  * Don't need an element to be inserted into the DOM before we can style it, it'll reflect those changes as soon as we interact with it
* Same args

### updated()

* `update(el, binding, vnode, oldVnode)`
* Called whenever the component updates
  * Without the children being updated yet
* Also get extra arg: `oldVnode`:
  * We get the new `vnode` and `oldVnode` in the virtual DOM since it updated

### componentUpdated()

* `componentUpdated(el, binding, vnode, oldVnode)`
* Called once the component has been updated
  * The children have also been updated

### unbind()

* `unbind(el, binding, vnode)`
* Called whenever the directive is removed

### Other Notes

* You will use `bind()` and `unbind()` the most
* `bind()` is what triggers whatever the directive should trigger whenever applied to an element
* Diagram:
  * ![](images/2017-12-04-22-39-15.png)

## 12.3 - Creating a Simple Directive

`src/main.js`

```javascript
Vue.directive('highlight', {
  bind(el, binding, vnode) {
    el.style.backgroundColor = 'green';
  }
});
```

* Don't need all the arguments here, but they're here just to show they can be used

`src/App.vue`

```html
        <h1>Custom Directives</h1>
        <p v-highlight>Color this</p>
```

* Output:
  * ![](images/2017-12-04-22-41-47.png)

## 12.4 - Passing Values to Custom Directives

`src/main.js`

```javascript
Vue.directive('highlight', {
  bind(el, binding, vnode) {
    el.style.backgroundColor = binding.value;
  }
});
```

`src/App.vue`

```html
        <h1>Custom Directives</h1>
        <p v-highlight="'red'">Color this</p>
```

* `binding` is the information about the directive binding (attached to `v-highlight`)
  * `binding.value` is whatever is between the quotes in `v-highlight="'red'"`

## 12.4 - Passing Arguments to Custom Directives

`src/App.vue`

```html
        <h1>Custom Directives</h1>
        <p v-highlight:background="'red'">Color this</p>
```

* In the directive, with a colon `:` we can pass in an argument
  * `:background` isn't a string, it's a normal "text" in the template
  * `background` is treated as an argument to the `v-highlight` directive

`src/main.js`

```javascript
Vue.directive('highlight', {
  bind(el, binding, vnode) {
    if (binding.arg === 'background') {
      el.style.backgroundColor = binding.value;
    } else {
      el.style.color = binding.value;
    }
  }
});
```

* `binding.arg` gets the argument passed to it
  * `background` must be a string b/c that's what it's converted as behind the scenes
* Output as is:
  * ![](images/2017-12-04-22-51-22.png)
* Output when you remove `:background` from the `v-highlight` binding
  * ![](images/2017-12-04-22-52-06.png)
  * The text color is red (which means there was no arg)

## 12.5 - Modifying a custom Directive with Modifiers

`src/main.js`

```javascript
Vue.directive('highlight', {
  bind(el, binding, vnode) {
    let delay = 0;
    if (binding.modifiers['delayed']) {
      delay = 3000;
    }
    setTimeout(() => {
      if (binding.arg === 'background') {
        el.style.backgroundColor = binding.value;
      } else {
        el.style.color = binding.value;
      }
    }, delay);
  }
});
```

`src/App.vue`

```html
        <h1>Custom Directives</h1>
        <p v-highlight:background.delayed="'red'">Color this</p>
```

* `binding.modifiers['delayed']` gets the `delayed` modifier attached to the binding
* Output:
  * ![](images/2017-12-04-22-56-58.png)
  * Same as before, but it takes a couple of seconds before the styling is applied

## 12.6 - Registering Directives Locally

`src/App.vue`

```html
<template>
...
        <h1>Custom Directives</h1>
        <p v-highlight:background.delayed="'red'">Color this</p>
        <p v-local-highlight:background.delayed="'red'">Color this too</p>
...
</template>
<script>
  export default {
    directives: {
      'local-highlight': {
        bind(el, binding, vnode) {
          let delay = 0;
          if (binding.modifiers['delayed']) {
            delay = 3000;
          }
          setTimeout(() => {
            if (binding.arg === 'background') {
              el.style.backgroundColor = binding.value;
            } else {
              el.style.color = binding.value;
            }
          }, delay);
        }
      }
    }
  }
</script>
```

* This is how we can register a local directive (use the `directives` property)

## 12.7 - Using Multiple Modifiers

`src/App.vue`

```html
<template>
...
        <h1>Custom Directives</h1>
        <p v-highlight:background.delayed="'red'">Color this</p>
        <p v-local-highlight:background.delayed.blink="'red'">Color this too</p>
...
</template>
<script>
  export default {
    directives: {
      'local-highlight': {
        bind(el, binding, vnode) {
          let delay = 0;
          if (binding.modifiers['delayed']) {
            delay = 3000;
          }
          if (binding.modifiers['blink']) {
            let mainColor = binding.value;
            let secondColor = 'blue';
            let currentColor = mainColor;

            setTimeout(() => {
              setInterval(() => {
                currentColor === secondColor ?
                  currentColor = mainColor :
                  currentColor = secondColor;
                if (binding.arg === 'background') {
                  el.style.backgroundColor = currentColor;
                } else {
                  el.style.color = currentColor;
                }
              },1000);
            }, delay);
          } else {
            setTimeout(() => {
              if (binding.arg === 'background') {
                el.style.backgroundColor = binding.value;
              } else {
                el.style.color = binding.value;
              }
            }, delay);
          }
        }
      }
    }
  }
</script>
```

* Here, we used another modifier `blink` which will blink the background color between red and blue every second:
  * ![](images/2017-12-04-23-08-32.png)

## 12.8 - Passing More Complex Values to Directives

`src/App.vue`

```html
<template>
...
        <h1>Custom Directives</h1>
        <p v-highlight:background.delayed="'red'">Color this</p>
        <p v-local-highlight:background.delayed.blink="{mainColor: 'red', secondColor: 'green', delay: 500}">Color this too</p>
...
</template>
<script>
  export default {
    directives: {
      'local-highlight': {
        bind(el, binding, vnode) {
          let delay = 0;
          if (binding.modifiers['delayed']) {
            delay = 3000;
          }
          if (binding.modifiers['blink']) {
            let mainColor = binding.value.mainColor;
            let secondColor = binding.value.secondColor;
            let currentColor = mainColor;

            setTimeout(() => {
              setInterval(() => {
                currentColor === secondColor ?
                  currentColor = mainColor :
                  currentColor = secondColor;
                if (binding.arg === 'background') {
                  el.style.backgroundColor = currentColor;
                } else {
                  el.style.color = currentColor;
                }
              },binding.value.delay);
            }, delay);
          } else {
            setTimeout(() => {
              if (binding.arg === 'background') {
                el.style.backgroundColor = binding.value.mainColor;
              } else {
                el.style.color = binding.value.mainColor;
              }
            }, delay);
          }
        }
      }
    }
  }
</script>
```

* Here, we changed the code to access `binding.value.mainColor` and so on
* Output:
  * ![](images/2017-12-04-23-13-29.png)
  * Blinks between red and green every half-second