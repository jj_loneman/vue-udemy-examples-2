# 7 - An Introduction to Components

## 7.1 - Introduction

`html`

```html
<script src="https://unpkg.com/vue/dist/vue,js"></script>

<div id="app">
  <my-cmp></my-cmp>
</div>
```

`js`

```javascript
Vue.component('my-cmp');

new Vue({
  el: '#app',
  data: {
    status: 'Critical'
  },
  template: '<p>Server Status: {{ status }}</p>'
});
```

* Calling `Vue.component()` kind of creates a new Vue instance with some new features
  * First argument is tag name of element you want to create
  * When creating components, it's a good idea to add a prefix to the name so it doesn't conflict with other existing ones or even future HTML elements
    * Use something like 'app' or 'my'

### New Code

`js`

```javascript
Vue.component('my-cmp', {
  data: {
    status: 'Critical'
  },
  template: '<p>Server Status: {{ status }}</p>'
});

new Vue({
  el: '#app'
});
```

* If we moved the `data` object to the component, we interfere with the other data objects of the root Vue instance

### Newer Code

`js`

```javascript
Vue.component('my-cmp', {
  data: function() {
    return {
      status: 'Critical'
    }
  },
  template: '<p>Server Status: {{ status }}</p>'
});

new Vue({
  el: '#app'
});
```

* When the data that we want to return is wrapped in a `function`, it won't interfere with data from the root instance

Output:
![](images/2017-11-29-00-51-01.png)

## 7.2 - Storing Data in Components with the Data Method

`html`

```html
<script src="https://unpkg.com/vue/dist/vue,js"></script>

<div id="app">
  <my-cmp></my-cmp>
  <hr>
  <my-cmp></my-cmp>
</div>
```

`js`

```javascript
var data = { status: 'Critical' };

Vue.component('my-cmp', {
  data: function() {
    return data;
  },
  template: '<p>Server Status: {{ status }} (<button @click="changeStatus">Change</button>)</p>',
  methods: {
    changeStatus: function() {
      this.status = 'Normal';
    }
  }
});

new Vue({
  el: '#app'
});
```

Output:
![](images/2017-11-30-15-21-59.png)

* If we create a `data` object and return the same object, then the component will refer to the same object (referenced in memory, so exactly the same object):
  * ![](images/2017-11-30-15-24-45.png)
  * When we click `Change` in the rendered HTML app, it changes both texts in boths components to 'Normal', which is the issue with shared objects
  * All methods that use `this.status` reference the same data

### New Code

`js`

```javascript
Vue.component('my-cmp', {
  data: function() {
    return {
      status: 'Critical'
    }
  },
  template: '<p>Server Status: {{ status }} (<button @click="changeStatus">Change</button>)</p>',
  methods: {
    changeStatus: function() {
      this.status = 'Normal';
    }
  }
});

new Vue({
  el: '#app'
});
```

* Now, when you click the button, it will only change that object's data:
  * ![](images/2017-11-30-15-27-17.png)
* The function in `data` gets executed for each separate component:
  * ![](images/2017-11-30-15-28-02.png)
* The keyword `this` only refers to the instance's member object
* Each Vue component is its own object

## 7.3 - Registering Components Locally and Globally

`html`

```html
<script src="https://unpkg.com/vue/dist/vue,js"></script>

<div id="app">
  <my-cmp></my-cmp>
  <hr>
  <my-cmp></my-cmp>
</div>

<div id="app2">
  <my-cmp></my-cmp>
  <hr>
  <my-cmp></my-cmp>
</div>
```

`js`

```javascript
Vue.component('my-cmp', {
  data: function() {
    return {
      status: 'Critical'
    }
  },
  template: '<p>Server Status: {{ status }} (<button @click="changeStatus">Change</button>)</p>',
  methods: {
    changeStatus: function() {
      this.status = 'Normal';
    }
  }
});

new Vue({
  el: '#app'
});

new Vue({
  el: '#app2'
});
```

* Having two Vue instances produces this output:
  * ![](images/2017-11-30-15-40-12.png)

### New Code

`js`

```javascript
var cmp = {
  data: function() {
    return {
      status: 'Critical'
    }
  },
  template: '<p>Server Status: {{ status }} (<button @click="changeStatus">Change</button>)</p>',
  methods: {
    changeStatus: function() {
      this.status = 'Normal';
    }
  }
};

new Vue({
  el: '#app',
  components: {
    'my-cmp': cmp
  }
});

new Vue({
  el: '#app2'
});
```

* `Vue.component(component_name, {component_object})` registers a component globally
* By creating a local `cmp` component object, we can register the object to the `#app` Vue instance in the `components` method
* This will only render the first `div`:
  * ![](images/2017-11-30-15-43-45.png)

## 7.4 - The "Root Component" in the App.vue File

`src/main.js`

```javascript
import Vue from 'vue'
import App from './App.vue'

new Vue({
  el: '#app',
  render: h => h(App)
});
```

* The code gets rendered in the `render()` method
  * Which is a Vue method which allows us to override the template select by the `el` selector and place the compiled template there
  * `render` is an alternative to `template` but a better template b/c we render the compiled JS code
    * Which means we don't face the limitations that `template` has, like requiring a string and accessing the DOM
* We kind of use the `h => h(App)` as a component even though we don't have a selector for it
* The root Vue instance gets rendered here like a component which isn't placed with a selector, but is like a replacement for the instance object in `new Vue()`
  * The `render()` function replaces everythin

`src/App.vue`

```html
<template>
  <h1>Server Status: {{ status }}</h1>
</template>

<script>
export default {
  data() {
    return {
      status: 'Critical'
    }
  }
}
</script>

<style>
</style>
```

* This will get compiled to a JS template plus the vue code
* The object exported in `export default` is not a component - it's a separate object being exported
* This is root Vue instance

### Output

![](images/2017-11-30-18-28-31.png)

## 7.5 - Creating a Component

`src/App.vue`

```html
<template>
  <app-server-status></app-server-status>
</template>

<script></script>

<style></style>
```

* You can use `app-server-status` since it's defined in `main.js`

`src/Home.vue`

```html
<template>
  <div>
    <p>Server Status: {{ status }}</p>
    <hr>
    <button @click="changeStatus">Change Status</button>
  </div>
</template>

<script>
  export default {
    data() {
      return {
        status: 'Critical'
      }
    },
    methods: {
      changeStatus() {
        this.status = 'Normal';
      }
    }
  }
</script>
```

* **Everything needs to be wrapped in one root element (usually a `div`)**
  * If it's not, you will get a compilation error

`src/main.js`

```javascript
import Vue from 'vue'
import App from './App.vue'
import Home from './Home.vue'

Vue.component('app-server-status', Home);

new Vue({
  el: '#app',
  render: h => h(App)
});
```

* In `Vue.component()`, it takes a unique selector and the Vue object (`Home`)
* The third import with `import Home ...`, you can replace `Home` with anything you want since it's not explictly defined in the `export default` function in `Home.vue`
* With this, you can use `app-server-status` as a selector in the whole app

### Output

![](images/2017-11-30-19-04-28.png)

* Clicking the button changes the status from `Critical` to `Normal`

## 7.6 - Using Components

`src/App.vue`

```html
<template>
  <app-servers></app-servers>
</template>

<script></script>

<style></style>
```

* This is also changed from `app-server-status` to `app-servers` to use the new selector that was set up globally

`src/Home.vue`

```html
<template>
  <div>
    <app-server-status v-for="server in 5"></app-server-status>
  </div>
</template>

<script>
  import ServerStatus from './ServerStatus.vue'
  export default {
    components: {
      'app-server-status': ServerStatus
    }
  }
</script>
```

* In `Home`, only want to loop through all the servers we have
* Here, `app-server-status` is defined locally which refers to the `ServerStatus` component
* In order to use it, you need to import the component (which you can do as of ES6)
  * Technically, the name can be whatever (doesn't have to be `ServerStatus`) since it's not defined in the component
* The `v-for="server in 5"` loops from 0 to 5, with `server` as the current counter
* We need to wrap the `<app-server-status>` tag in a `<div>` since the loop will end up creating 5 elements, and we can only have one root element

`src/ServerStatus.vue`

```html
<template>
  <div>
    <p>Server Status: {{ status }}</p>
    <hr>
    <button @click="changeStatus">Change Status</button>
  </div>
</template>

<script>
  export default {
    data() {
      return {
        status: 'Critical'
      }
    },
    methods: {
      changeStatus() {
        this.status = 'Normal';
      }
    }
  }
</script>
```

* The name of this component is irrelevant for the name of the selector you'll end up choosing
  * This is because you'll set up the selector in `main.js` in the `Vue.component()` method or
  * ..as a string in the component's properties

`src/main.js`

```javascript
import Vue from 'vue'
import App from './App.vue'
import Home from './Home.vue'

Vue.component('app-servers', Home);

new Vue({
  el: '#app',
  render: h => h(App)
});
```

* Here, `app-servers` is defined globally (renamed from `app-server-status` so there aren't conflicts)

### Output

![](images/2017-11-30-19-53-34.png)

## 7.7 - Moving to a Better Folder Structure

* Should put components into a `components` directory, with features in their own directories
  * ![](images/2017-11-30-23-17-29.png)

## 7.8 - Alternative Folder Structures

* Group files by features
* main.js
* users/
  * account/
  * analytics/
* shop
  * main/
  * checkout/

## 7.9 - How to Name Component Tags (Selectors)

* Could name camelCase and have the tag hyphen-case since 

`App.vue`

```html
<template>
  <div class="container">
    <app-header></app-header>
    <hr>
    <div class="row">
      <servers></servers>
      <app-server-details></app-server-details>
    </div>
    <hr>
    <app-footer></app-footer>
  </div>
</template>

<script>
  import Header from './components/Shared/Header.vue';
  import Footer from './components/Shared/Footer.vue';
  import Servers from './components/Server/Servers.vue';
  import ServerDetails from './components/Server/ServerDetails.vue';

  export default {
    components: {
      appHeader: Header,
      Servers,
      'app-server-details': ServerDetails,
      'app-footer': Footer
    }
  }
</script>

<style>

</style>
```

* `Servers` in the `components` method is the same as `Servers: Servers`, which is an ES6 thing

## 7.10 - Scoping Component Styles

In `ServerDetails.vue`, if we have:

```html
<style>
  div {
    border: 1px solid red;
  }
</style>
```

In `Servers.vue`, if we have:

```html
<style>
  div {
    border: 1px solid blue;
  }
</style>
```

we get this:

![](images/2017-12-01-00-01-20.png)

* This is because the style isn't scoped
  * Any style that's set in any component will be applied globally in the global stylesheet which may not be the behavior you want
  * Since `ServerDetails` is loaded first, it takes precedence
  * You can override that by applying the `scoped` attribute to the `<style>` tag

### New Code

`ServerDetails.vue`

```html
<style scoped>
  div {
    border: 1px solid red;
  }
</style>
```

`Servers.vue`

```html
<style scoped>
  div {
    border: 1px solid blue;
  }
</style>
```

Output:

![](images/2017-12-01-00-07-30.png)

* Behind the scenes, `scoped` emulates the "shadow DOM"
  * The "shadow DOM" means in upcoming browsers, the DOM in each element has a DOM behind each element
    * The `<h1>` tag might have a different style than another `<h1>` tag
  * In this example, the first `<div>` has a different attribute `data-v-...` than the second one
    * ![](images/2017-12-01-00-21-48.png)
* All the styles are pulled up to the `<head>` of the HTML:
  * ![](images/2017-12-01-00-22-56.png)
  * In the `<style>` tag, it selects the elements by the attribute:
    * ![](images/2017-12-01-00-25-02.png)