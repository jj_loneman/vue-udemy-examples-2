# 17 - Better State Management with Vuex

## 17.1 - Why a Different State Management May Be Needed

![](images/2017-12-05-07-38-59.png)

* This gets messy with larger apps

![](images/2017-12-05-07-40-05.png)

* An alternative is to use an event bus
* One bus will quickly get crowded in bigger apps
* Changes are hard to track when they're accessed all over the app

## 17.2 - Understanding "Centralized State"

![](images/2017-12-05-07-43-51.png)

* One file in the app that stores the app state
  * With this, one child could change the data in the sate
  * Another could get the data

## 17.3 - Using the Centralized State

`src/store/store.js`

```javascript
import Vue from 'vue';
import Vuex from 'vuex';

Vue.use(Vuex);

export const store = new Vuex.Store({
  state: {
    counter: 0
  }
});
```

* Typically store all Vuex-related parts in `src/store`
* Object passed into `Store()` must be called `state`

`src/main.js`

```javascript
import Vue from 'vue'
import App from './App.vue'

import { store } from './store/store';

new Vue({
  el: '#app',
  store,
  render: h => h(App)
});
```

* Here, `store` is registered with the app

`src/components/Counter.vue`

```html
<script>
  export default {
    methods: {
      increment() {
        this.$store.state.counter++;
      },
      decrement() {
        this.$store.state.counter--;
      }
    }
  }
</script>
```

`src/components/Result.vue`

```javascript
<template>
  <p>Counter is: {{ counter }}</p>
</template>

<script>
  export default {
    computed: {
      counter() {
        return this.$store.state.counter;
      }
    }
  }
</script>
```

`src/App.vue`

```html
<template>
  <div class="container">
    <div class="row">
      <div class="col-xs-12 col-sm-8 col-sm-offset-2 col-md-6 col-md-offset-3">
        <h1>Vuex</h1>
        <app-result></app-result>
        <hr>
        <app-counter></app-counter>
      </div>
    </div>
  </div>
</template>

<script>
  import Counter from './components/Counter.vue';
  import Result from './components/Result.vue';

  export default {
    components: {
      appCounter: Counter,
      appResult: Result,
    }
  }
</script>
```

* Output:
  * ![](images/2017-12-05-07-54-00.png)

## 17.4 - Why a Centralized State Alone Won't Fix It

`src/components/AnotherResult.vue`

```html
<template>
  <p>Counter is: {{ counter }}</p>
</template>

<script>
  export default {
    computed: {
      counter() {
        return this.$store.state.counter * 2;
      }
    }
  }
</script>
```

`src/App.vue`

```html
<template>
  <div class="container">
    <div class="row">
      <div class="col-xs-12 col-sm-8 col-sm-offset-2 col-md-6 col-md-offset-3">
        <h1>Vuex</h1>
        <app-result></app-result>
        <app-another-result></app-another-result>
        <hr>
        <app-counter></app-counter>
      </div>
    </div>
  </div>
</template>

<script>
  import Counter from './components/Counter.vue';
  import Result from './components/Result.vue';
  import AnotherResult from './components/AnotherResult';

  export default {
    components: {
      appCounter: Counter,
      appResult: Result,
      appAnotherResult: AnotherResult
    }
  }
</script>
```

* It correctly updates
* But the issue is we have duplicate code everywhere
  * Hard to manage - if we have an error in our calculation, you'd have to change it in a bunch of files
* Output
  * ![](images/2017-12-05-07-58-41.png)

## 17.5 - Understanding Getters

![](images/2017-12-05-08-02-01.png)

* Instead of directly accessing the state, we can use a getter
* The calculation is stored in one place, but we can use the getter to get the value

## 17.6 - Using Getters

`src/store/store.js`

```javascript
import Vue from 'vue';
import Vuex from 'vuex';

Vue.use(Vuex);

export const store = new Vuex.Store({
  state: {
    counter: 0
  },
  getters: {
    doubleCounter: state => {
      return state.counter * 2;
    }
  }
});
```

* Must be named `getters` as it's a reserved name

`src/components/AnotherResult.vue`

```html
<template>
  <p>Counter is: {{ counter }}</p>
</template>

<script>
  export default {
    computed: {
      counter() {
        return this.$store.getters.doubleCounter;
      }
    }
  }
</script>
```

`src/components/Result.vue`

```html
<template>
  <p>Counter is: {{ counter }}</p>
</template>

<script>
  export default {
    computed: {
      counter() {
        return this.$store.getters.doubleCounter;
      }
    }
  }
</script>
```

* You don't need to execute `doubleCounter` as a function as Vuex will do it for you

## 17.7 - Mapping Getters to Properties

`src/components/AnotherResult.vue`

```html
<template>
  <div>
    <p>Counter is: {{ doubleCounter }}</p>
    <p>Numer of Clicks: {{ stringCounter }}</p>
  </div>
</template>

<script>
  import {mapGetters} from 'vuex';

  export default {
    computed: {
      ...mapGetters([
        'doubleCounter',
        'stringCounter'
      ])
    }
  }
</script>
```

* In the background, you'll automatically have computed properties and will map them to the functions in the store
* You can pass an object and map the getters to different names
  ```javascript
  mapGetters({
    propertyName: 'doubleCounter'
  })
  ```
* Spread operator:
  ```javascript
      computed: {
      ...mapGetters([
        'doubleCounter',
        'stringCounter'
      ])
    }
  ```
  * If you want to have something else in `computed`, you can't just stick `mapGetters` in since `mapGetters` creates computed methods
  * The spread operator allows us to tell JS to pull out all the properties and methods in `mapGetters` and create separate key-value pairs for each of them
  * For this project, since it transpiles to ES5, the app won't work because it doesn't recognize the spread operator

* In order to use the spread operator, need to download another package:

```commandline
npm install --save-dev babel-preset-stage-2
```

`.babelrc`

```javascript
{
  "presets": [
    ["es2015", { "modules": false }],
    ["stage-2"]
  ]
}
```

* Output:
  * ![](images/2017-12-05-08-07-40.png)

## 17.8 - Understanding Mutations

![](images/2017-12-05-08-21-35.png)

* If we have multiple components maniuplating the state, it can get hard to know which component updated what
* Better way is to use a similar concept for getters: mutations
* Mutations are committed
  * They update the state
  * Any components that listen to the state will receive an update

## 17.9 - Using Mutations

`src/store/store.js`

```javascript
...
  mutations: {
    increment: state => {
      state.counter++;
    },
    decrement: state => {
      state.counter--;
    }
  }
```

`src/components/AnotherCounter.vue`

```html
<script>
  import { mapMutations } from 'vuex';

  export default {
    methods: {
      ...mapMutations([
        'increment',
        'decrement'
      ])
    }
  }
</script>
```

* Output:
  * ![](images/2017-12-05-08-27-01.png)

## 17.10 - Why Mutations have to run Synchronously

* They must be synchronous, otherwise the main benefit of having a central state gets lost
  * If async, the state might change from one mutation and then change again from a different source

## 17.11 - How Actions improve Mutations

![](images/2017-12-05-08-31-01.png)

* Mutations must change state immediately
* You can fix this by putting extra piece between Mutation and state
* An action is an extra function where you may want to run async task
* All the changes still happens synchronously, but async stuff might happen before the changes are committed

## 17.12 - Using Actions

`src/store/store.js`

```javascript
  actions: {
    increment: context => {
      context.commit('increment');
    }
  }
```

or

```javascript
  actions: {
    increment: ({ commit }) => {
      commit('increment');
    }
  }
```

* `context` gives us access to `commit()` method and `getters` and so on
* This doesn't really do anything for us - is just an extra step
* Still good practice to have actions commit mutations
* You only need to use actions when you need to use async tasks

`src/store/store.js`

```javascript
  actions: {
    increment: ({ commit }) => {
      commit('increment');
    },
    decrement: ({ commit }) => {
      commit('decrement');
    },
    syncIncrement: ({ commit }) => {
      setTimeout(() => {
        commit('increment');
      }, 1000);
    },
    syncDecrement: ({ commit }) => {
      setTimeout(() => {
        commit('decrement');
      }, 1000);
    }
  }
```

* There will always be a second delay before increment and decrement happen

`src/components/AnotherCounter.vue`

```html
<script>
  import { mapActions } from 'vuex';

  export default {
    methods: {
      ...mapActions([
        'increment',
        'decrement'
      ])
    }
  }
</script>
```

## 17.13 - Mapping Actions to Methods

`src/components/AnotherCounter.vue`

```html
<template>
  <div>
    <button
        class="btn btn-primary"
        @click="asyncIncrement({by: 50, duration: 500})"
    >
      Increment
    </button>
    <button
        class="btn btn-primary"
        @click="asyncDecrement({by: 50, duration: 500})"
    >
      Decrement
    </button>
  </div>
</template>

<script>
  import { mapActions } from 'vuex';

  export default {
    methods: {
      ...mapActions([
        'asyncIncrement',
        'asyncDecrement'
      ])
    }
  }
</script>
```

`src/store/store.js`

```javascript
import Vue from 'vue';
import Vuex from 'vuex';

Vue.use(Vuex);

export const store = new Vuex.Store({
  state: {
    counter: 0
  },
  getters: {
    doubleCounter: state => {
      return state.counter * 2;
    },
    stringCounter: state => {
      return state.counter + ' Clicks';
    }
  },
  mutations: {
    increment: (state, payload) => {
      state.counter += payload;
    },
    decrement: (state, payload) => {
      state.counter -= payload;
    }
  },
  actions: {
    increment: ({ commit }, payload) => {
      commit('increment', payload);
    },
    decrement: ({ commit }, payload) => {
      commit('decrement', payload);
    },
    asyncIncrement: ({ commit }, payload) => {
      setTimeout(() => {
        commit('increment', payload.by);
      }, payload.duration);
    },
    asyncDecrement: ({ commit }, payload) => {
      setTimeout(() => {
        commit('decrement', payload.by);
      }, payload.duration);
    }
  }
});
```

* This is how we can handle multiple pieces of data going into a payload
* This allows us to pass payloads to actions to mutations

## 17.14 - Summary

![](images/2017-12-05-08-51-43.png)

* Central store gets updated by mutations
  * Mutations must be synchronous
* Therefore, often times use actions to faciliate mutations
  * Good practice to always use actions even for non-async tasks
* Getters allow you to access state in different components
  * Have benefit of components getting updated whenever the state changes

### Getters

`src/store/store.js`

* State
  ![](images/2017-12-05-08-54-22.png)
* Getters
  ![](images/2017-12-05-08-55-02.png)

`src/components/AnotherResult.vue`

![](images/2017-12-05-08-56-33.png)

* Use `mapGetters`: a helper method used to create computed properties automatically to access our state or properties of the state

`src/components/Result.vue`

![](images/2017-12-05-08-57-00.png)

* We access the property directly since we only use one getter

### Mutations

`src/store/store.js`

![](images/2017-12-05-08-57-59.png)

* Mutations take in a state as input and a payload (optional)
* Here, we directly manipulate the state and overwrite it

### Actions

`src/store/store.js`

![](images/2017-12-05-08-59-03.png)

* Lots of times, we want to use actions if we want to run async tasks
* First argument to action is context
  * Context is almost the same as the store (some minor differences)
  * But we really only need to commit method
* You can use async code in an action as long as the committing is done synchronously

`src/components/AnotherCounter.vue`

![](images/2017-12-05-09-01-23.png)

* We use `mapActions` to let Vuex create the actions to access our actions

## 17.15 - Two-Way-Binding (v-model) and Vuex

`src/store/store.js`

```javascript
import Vue from 'vue';
import Vuex from 'vuex';

Vue.use(Vuex);

export const store = new Vuex.Store({
  state: {
    counter: 0,
    value: 0
  },
  getters: {
    doubleCounter: state => {
      return state.counter * 2;
    },
    stringCounter: state => {
      return state.counter + ' Clicks';
    },
    value: state => {
      return state.value;
    }
  },
  mutations: {
    increment: (state, payload) => {
      state.counter += payload;
    },
    decrement: (state, payload) => {
      state.counter -= payload;
    },
    updateValue: (state, payload) => {
      state.value = payload;
    }
  },
  actions: {
    increment: ({ commit }, payload) => {
      commit('increment', payload);
    },
    decrement: ({ commit }, payload) => {
      commit('decrement', payload);
    },
    asyncIncrement: ({ commit }, payload) => {
      setTimeout(() => {
        commit('increment', payload.by);
      }, payload.duration);
    },
    asyncDecrement: ({ commit }, payload) => {
      setTimeout(() => {
        commit('decrement', payload.by);
      }, payload.duration);
    },
    updateValue({ commit }, payload) {
      commit('updateValue', payload);
    }
  }
});
```

* Added methods for updating `value`

`src/App.vue`

```html
<template>
  <div class="container">
    <div class="row">
      <div class="col-xs-12 col-sm-8 col-sm-offset-2 col-md-6 col-md-offset-3">
        <h1>Vuex</h1>
        <app-result></app-result>
        <app-another-result></app-another-result>
        <hr>
        <app-counter></app-counter>
        <app-another-counter></app-another-counter>
        <hr>
        <input type="text" v-model="value">
        <p>{{ value }}</p>
      </div>
    </div>
  </div>
</template>

<script>
  import Counter from './components/Counter.vue';
  import Result from './components/Result.vue';
  import AnotherResult from './components/AnotherResult';
  import AnotherCounter from './components/AnotherCounter';

  export default {
    computed: {
      value: {
        get() {
          return this.$store.getters.value;
        },
        set(value) {
          this.$store.dispatch('updateValue', value);
        }
      }
    },
    methods: {
      updateValue(event) {
        this.$store.dispatch('updateValue', event.target.value);
      }
    },
    components: {
      appCounter: Counter,
      appResult: Result,
      appAnotherResult: AnotherResult,
      appAnotherCounter: AnotherCounter
    }
  }
</script>
```

* Using a setter in a computed property is something you rarely need
* `computed` means it depends on something else and gets re-calculated
* This is one of the rare cases
* We do that so we can use `v-model`
* Otherwise, we'd have:
  ```html
  <input type="text" :value="value" @input="updateValue">
  ```
  and use the method `updateValue()`

## 17.16 - Modularizing the State Management

* The `counter` and all related getters and actions belong to its own `counter` file

`src/modules/counter.js`

```javascript
const state = {
  counter: 0,
};

const getters = {
  doubleCounter: state => {
    return state.counter * 2;
  },
  stringCounter: state => {
    return state.counter + ' Clicks';
  },
};

const mutations = {
  increment: (state, payload) => {
    state.counter += payload;
  },
  decrement: (state, payload) => {
    state.counter -= payload;
  },
};

const actions = {
  increment: ({ commit }, payload) => {
    commit('increment', payload);
  },
  decrement: ({ commit }, payload) => {
    commit('decrement', payload);
  },
  asyncIncrement: ({ commit }, payload) => {
    setTimeout(() => {
      commit('increment', payload.by);
    }, payload.duration);
  },
  asyncDecrement: ({ commit }, payload) => {
    setTimeout(() => {
      commit('decrement', payload.by);
    }, payload.duration);
  },
};

export default {
  state,
  mutations,
  actions,
  getters,
}
```

`src/store/store.js`

```javascript
import Vue from 'vue';
import Vuex from 'vuex';
import counter from './modules/counter';

Vue.use(Vuex);

export const store = new Vuex.Store({
  state: {
    value: 0
  },
  getters: {
    value: state => {
      return state.value;
    }
  },
  mutations: {
    updateValue: (state, payload) => {
      state.value = payload;
    }
  },
  actions: {
    updateValue({ commit }, payload) {
      commit('updateValue', payload);
    }
  },
  modules: {
    counter
  }
});
```

* You can use the `modules` object to take in other modules

## 17.17 - Using Separate Files

`src/store/store.js`

```javascript
import Vue from 'vue';
import Vuex from 'vuex';
import counter from './modules/counter';

import * as actions from './actions';
import * as getters from './getters';
import * as mutations from './mutations';

Vue.use(Vuex);

export const store = new Vuex.Store({
  state: {
    value: 0
  },
  getters,
  mutations,
  actions,
  modules: {
    counter
  }
});
```

* `import * as actions` lets JS create an object called actions
  * All the properties of that object are accessible
* Can just access it by called `actions`

`src/store/actions.js`

```javascript
export const updateValue = ({ commit }, payload) => {
  commit('updateValue', payload);
};
```

`src/store/getters.js`

```javascript
export const value = state => {
  return state.value;
};
```

`src/store/mutations.js`

```javascript
export const updateValue = (state, payload) => {
  state.value = payload;
};
```

* You could even go further and create a `src/store/modules/counter` directory where `counter` has `actions`, `getters`, `mutations`, etc.

## 17.18 - Using Namespaces to Avoid Naming Problems

`src/store/types.js`

```javascript
export const DOUBLE_COUNTER = 'counter/DOUBLE_COUNTER';
export const CLICK_COUNTER = 'counter/CLICK_COUNTER';
```

* CAPS naming convention for globals
* By using a prefix here, you ensure the strings are unique
* You could also set this up with all the getters, mutations, actions, etc.
* You might want to do this if you have a large app

`src/store/store.js`

```javascript
import * as types from '../types';

const state = {
  counter: 0,
};

const getters = {
  [types.DOUBLE_COUNTER]: state => {
    return state.counter * 2;
  },
  [types.CLICK_COUNTER]: state => {
    return state.counter + ' Clicks';
  },
};
```

* The `[]` notation means the name can be replaced at runtime

`src/components/AnotherResult.vue`

```html
<script>
  import {mapGetters} from 'vuex';
  import * as types from '../store/types';

  export default {
    computed: {
      ...mapGetters({
        doubleCounter: types.DOUBLE_COUNTER,
        stringCounter: types.CLICK_COUNTER
      })
    }
  }
</script>
```

`src/components/Result.vue`

```html
<script>
  import {mapGetters} from 'vuex';
  import * as types from '../store/types';

  export default {
    computed: {
      ...mapGetters({
        counter: types.DOUBLE_COUNTER,
      })
    }
  }
</script>
```

## 17.19 - Auto-namespacing with Vuex 2.1

* If using Vuex v2.1+, you can use auto-namespacing feature to avoid having to set up all the namespaces manually
* <https://github.com/vuejs/vuex/releases/tag/v2.1.0>
* Modules can be auto-namescpaced using `namespaced: true` option
  * The getters, actions, and mutations inside a namespaced module will automatically be prefix with a namespaced inferred from the module's registration path

```javascript
  const store = new Vuex.Store({
    modules: {
      foo: {
        namespaced: true,
        // ...
      }
    }
  })
```

* In this example, the `foo` module will automatically get assigned the namespace `foo/`
* Everything inside the module are auto-resolved to respect the namespace
  * So toggling between namespaced or not doesn't affect the code in the module itself

* Helpers for namespaced modules
  * You can pass a namespace string as the first arg to `mapState`, `mapGetters`, `mapActions`, and `mapMutations` helpers, so the mappings are resolved using that module as the context

```javascript
computed: {
  ...mapState('foo', {
    // state is the state of the `foo/` module instead of root
    bar: state => state.bar
  }
},
methods: {
  ...mapActions('foo', [
    // map this.doSomething() to this.$store.dispatch('foo/doSomething')
    'doSomething'
  ])
}
```

## 17.20 - Resources and Links

* Vuex Github: <https://github.com/vuejs/vuex>
* Vuex Docs: <https://vuex.vuejs.org/en/>