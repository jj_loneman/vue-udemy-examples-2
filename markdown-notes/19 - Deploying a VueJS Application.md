# 19 - Deploying a VueJS Application

## 19.1 - Introduction

```commandline
npm run build
```

* Don't start a dev server, it'll prepare the project for deployment
  * It will create a `dist` directory with `build.js` file
    * All files from the `src` directory are bundled into one big file

## 19.2 - Preparing for Deployment

* In `webpack.config.js`

### DefinePlugin

* ![](images/2017-12-05-09-52-31.png)
* In `DefinePlugin()`, it sets the mode to `production` which strips out some warnings that Vue would throw otherwise
* This reduces the size of the final file

### UglifyJSPlugin

* ![](images/2017-12-05-09-54-43.png)
* Minifies everything - makes everything more compact

## 19.3 - Deploying the App (Amazon S3 Example)

* Amazon S3 is a cloud storage service, but it also allows you to host SPAs where you don't need to run any server-side code

* Click S3
  * ![](images/2017-12-05-09-57-29.png)
* Create Bucket
  * ![](images/2017-12-05-09-57-52.png)
* Name your bucket
  * ![](images/2017-12-05-09-58-11.png)
* On the right-hand side, click "Static Website Hosting" and enable it, setting `index.html` to both index and error document and click save
  * ![](images/2017-12-05-09-59-46.png)
* Click "Permissions" and "Add bucket policy"
  ![](images/2017-12-05-10-00-24.png)
  * Google "aws static webpage"
  * Click link:
    * ![](images/2017-12-05-10-02-56.png)
  * Scroll down to "Configure Your Buckets" link:
    * ![](images/2017-12-05-10-03-20.png)
  * Copy the policy and paste into "Bucket Policy Editor"
    * ![](images/2017-12-05-10-03-48.png)
  * Replace `example.com` with your bucket (`vuejs-deploy`)
* Click on the `vuejs-deploy` bucket
  * ![](images/2017-12-05-10-05-03.png)
* Click "Upload"
  * ![](images/2017-12-05-10-05-26.png)
  * Upload `index.html`
    * ![](images/2017-12-05-10-05-55.png)
  * And upload `build.js`
    * ![](images/2017-12-05-10-06-13.png)
* Click "Start Upload"
  * ![](images/2017-12-05-10-06-50.png)
* Create folder called "dist"
  * ![](images/2017-12-05-10-07-23.png)
  * Move `build.js` into `dist/` folder
* Go back to "all buckets", click your bucket -> properties -> static website hosting
  * Click the endpoint
    * ![](images/2017-12-05-10-08-51.png)
* Profit
  * ![](images/2017-12-05-10-09-08.png)