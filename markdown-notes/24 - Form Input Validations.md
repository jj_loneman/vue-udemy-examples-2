# 24 - Form Input Validation

## 24.1 - Installing Vuelidate

* Vue doesn't have built-in validation like Angular
* Vuelidate: <https://monterail.github.io/vuelidate>
* Do `npm install vuelidate --save`

## 24.2 - Adding a Validator

* To register Vuelidate in app:

`src/main.js`

```javascript
import Vuelidate from 'vuelidate';

Vue.use(Vuelidate);
```

`src/components/auth/signup.vue`

```html
<template>
...
          <input
              type="email"
              id="email"
              @input="$v.email.$touch()"
              v-model="email"
          >
...
</template>
<script>
  import { required, email } from 'vuelidate/lib/validators';
  ...
      validations: {
      email: {
        required,
        email
      }
    },
</script>
```

* Vuelidate requires that the name of the key being validated is the same one you're using in `v-model` so it can automatically synchronize the values and know when it changes
* Vuelidator validators: <https://monterail.github.io/vuelidate/#sub-builtin-validators>
* `$v` is a reserved word exposed by the Vuelidate pacakge
  * Gives us access to the validators, validation rules, and all behind-the-scenes validation
  * Bind it to `@input`

* ![](images/2017-12-06-22-45-43.png)
  * If we output `$v` in a div, we can see what the object is
  * `required` is `false`, meaning the validator is not met (it's empty)
* ![](images/2017-12-06-22-47-18.png)
  * After typing in `t`
  * `$dirty`: `true`
  * `required` is `true`
  * `$invalid` is `true`
  * `$error` is also `true`
    * `$error` is only true when both `invalid` and `dirty` are true, which provides a better UX to control displaying errors
  * `pending`: true if we're still evaluating the correctness of it (more important for async validators)

## 24.3 - Adding Validation UI Feedback

`src/components/auth/signup.vue`

```html
<template>
...
        <div class="input" :class="{invalid: $v.email.$error}">
          <label for="email">Mail</label>
          <input
              type="email"
              id="email"
              @input="$v.email.$touch()"
              v-model="email"
          >
          <p v-if="!$v.emaill.email">Please provide a valid email address.</p>
        </div>
...
</template>
...
<style>
...
  .input.invalid label {
    color: red;
  }

  .input.invalid input {
    border: 1px solid red;
    background-color: #ffc9aa;
  }
...
</style>
```

* When type in invalid email: style it red:
  * ![](images/2017-12-06-22-57-05.png)
  * Only does it when it's invalid
* `$v.email.email`
  * `true` when it's a valid email address

## 24.4 - Controlling Styles for Invalid Entries

![](images/2017-12-06-23-02-11.png)

* With this UX, the error message is displayed while the user is typing an email for the first time, not the best
* By using `@blur`, it only fires when the input loses focus
  * Only when you click out of text box after you've started typing, the text turns red
* You can call `$touch()` whenever you feel it's the right time to validate the input

`src/components/auth/signup.vue`

```html
          <input
              type="email"
              id="email"
              @blur="$v.email.$touch()"
              v-model="email"
          >
```

## 24.5 - More Validators

`src/components/auth/signup.vue`

```html
<template>
...
          <input
              type="number"
              id="age"
              v-model.number="age"
          >
          <p v-if="$v.age.minVal">You have to be at least {{ $v.age.$params.minVal.min }} years old.</p>
...
</template>
<script>
  import { required, email, numeric, minValue } from 'vuelidate/lib/validators';
  ...
    validations: {
      email: {
        required,
        email
      },
      age: {
        required,
        numeric,
        minVal: minValue(18)
      }
    },
...
</script>
```

* `$params` in `$v.age.$params` holds all the params for all the different validators, including `minVal`
* ![](images/2017-12-06-23-10-07.png)

## 24.6 - Validating Passwords

* One way of doing it: using `sameAs()` validator

`src/components/auth/signup.vue`

```html
<template>
...
        <div
            class="input"
            :class="{invalid: $v.password.$error}"
        >
          <label for="password">Password</label>
          <input
              type="password"
              id="password"
              @blur="$v.password.$touch()"
              v-model="password"
          >
        </div>
        <div class="input">
          <label
              for="confirm-password"
              :class="{invalid: $v.confirmPassword.$error}"
          >
            Confirm Password
          </label>
          <input
              type="password"
              id="confirm-password"
              @blur="$v.confirmPassword.$touch()"
              v-model="confirmPassword"
          >
        </div>
        ...
</template>
<script>
  import {required, email, numeric, minValue, minLength, sameAs} from 'vuelidate/lib/validators';
  ...
      validations: {
      email: {
        required,
        email
      },
      age: {
        required,
        numeric,
        minVal: minValue(18)
      },
      password: {
        required,
        minLen: minLength(6)
      },
      confirmPassword: {
        sameAs: sameAs('password')
      }
    },
    ...
</script>
```

* Another way of using `sameAs()`:
  * `sameAs()` takes in either one of two things:
    * Name of property you want to check against
    * Or function
* If we use the function way, we can add a custom validation rule:
  ```javascript
  sameAs: sameAs(vm => {
    return vm.password + 'b';
  })
  ```
  * So we could use this if we want to normalize input or convert it to uppercase or something

## 24.7 - Using the Required-Unless Validator

`src/components/auth/signup.vue`

```html
        <div
            class="input inline"
            :class="{invalid: $v.terms.$invalid}"
        >
          <input
              type="checkbox"
              id="terms"
              @change="$v.terms.$touch()"
              v-model="terms"
          >
          <label for="terms">Accept Terms of Use</label>
        </div>
```

```javascript
      terms: {
        required: requiredUnless(vm => {
          return vm.country === 'germany';
        })
      }
    },
```

* Maybe we want to require something conditionally
* An example is requiring the "terms and conditions" box to be checked unless the country is Germany or something
* We can make the terms invalid when the page loads so it's clear we need to check it before continuing
  * ![](images/2017-12-06-23-31-26.png)
* In any other country, terms box is red:
  * ![](images/2017-12-06-23-33-37.png)
* For Germany, terms is not red:
  * ![](images/2017-12-06-23-33-56.png)

## 24.8 - Validating Arrays

`src/components/auth/signup.vue`

```html
        <div class="hobbies">
          <h3>Add some Hobbies</h3>
          <button
              @click="onAddHobby"
              type="button"
          >Add Hobby
          </button>
          <div class="hobby-list">
            <div
                class="input"
                v-for="(hobbyInput, index) in hobbyInputs"
                :class="{invalid: $v.hobbyInputs.$each[index].$error}"
                :key="hobbyInput.id"
            >
              <label :for="hobbyInput.id">Hobby #{{ index }}</label>
              <input
                  type="text"
                  :id="hobbyInput.id"
                  @blur="$v.hobbyInputs.$each[index].value.$touch()"
                  v-model="hobbyInput.value"
              >
              <button
                  @click="onDeleteHobby(hobbyInput.id)"
                  type="button"
              >
                X
              </button>
            </div>
            <p v-if="!$v.hobbyInputs.minLen">You must specify at least {{ $v.hobbyInputs.$params.minLen.min }} hobbies.</p>
            <p v-if="$v.hobbyInputs.required">Please add hobbies.</p>
          </div>
```

```javascript
    validations: {
      ...  
      hobbyInputs: {
        required,
        minLen: minLength(2),
        $each: {
          value: {
            required,
            minLen: minLength(5)
          }
        }
      }
    },
  }
```

## 24.9 - Controlling the Form Submit Button

`src/components/auth/signup.vue`

```html
<button type="submit" :disabled="$v.$invalid">Submit</button>
```

* Here, the button will be disabled from the start and won't be enabled until the user meets all required conditions of the form

## 24.10 - Creating Custom Validators

`src/components/auth/signup.vue`

```javascript
    validations: {
      email: {
        required,
        email,
        unique: val => {
          return val !== 'test@test.com';
        }
      },
```

* An example of an async validator is reaching out to the server to see if an email address already exists
* In the `unique` validator, it takes in a function
  * The function takes in a value, automatically passed in by Vuelidator
  * It needs to return whether the validator is true or not
  * If return false, it means it's not valid
* When you type in `test@test.com`, it fails:
  * ![](images/2017-12-07-00-21-27.png)

## 24.11 - Async Validators

* In Firebase, can set this in the Rules for the db:

```json
{
  "rules": {
    ".read": "true",
    ".write": "auth != null",
    "users": {
      ".indexOn": ["email"]
    }
  }
}
```

* This tells Firebase that there is a key on `users` which I want to be able to query
* Also, `read` needs to be `true` so we can read the db even if we aren't authenticated

```javascript
        unique: val => {
          if (val === '') {
            return true;
          }
          return new Promise((resolve, reject) => {
            setTimeout(() => {
              resolve(val !== 'test@test.com');
            }, 1000);
          });
        }
```

* To make it async, just have to return a promise in the validator
* Important to have the blank value check because you don't want to code to run if the value is just empty

`src/components/auth/signup.vue`

```javascript
    validations: {
      email: {
        required,
        email,
        unique: val => {
          if (val === '') {
            return true;
          }
          return axios.get('/users.json?orderBy="email"&equalTo="' + val + '"')
            .then(res => {
              console.log(res);
              return Object.keys(res.data).length === 0;
            })
        }
      },
```

* `Object.keys()` gives us an array of all the keys the object has
* If our return statement is true, that means the user has picked a unique email
* You want to be careful when you execute the request
  * If you run the request on every keystroke, you run the danger of overloading your db
  * You can use something like `debounce` from `lodash` so it'll group requests with delays between to prevent that from happening

## 24.12 - Resources and Linkes

* Validate Docs: <https://monterail.github.io/vuelidate/>