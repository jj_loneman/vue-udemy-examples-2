# 22 - Using Axios instead of vue-resource

## 22.1 - Introduction

* Axios is popular package for making HTTP requests for JS apps
* An alternative to `vue-resource`
  * Nothing wrong with using `vue-resource`

## 22.3 - Axios Setup

```commandline
npm install --save axios
```

* Unlike `vue-router`, you don't have to use it on Vue app

## 22.4 - Sending a POST Request

`src/components/auth/signup.vue`

```html
<script>
  import axios from 'axios';

  export default {
    data() {
      return {
        email: '',
        age: null,
        password: '',
        confirmPassword: '',
        country: 'usa',
        hobbyInputs: [],
        terms: false
      }
    },
    methods: {
      onAddHobby() {
        const newHobby = {
          id: Math.random() * Math.random() * 1000,
          value: ''
        };
        this.hobbyInputs.push(newHobby)
      },
      onDeleteHobby(id) {
        this.hobbyInputs = this.hobbyInputs.filter(hobby => hobby.id !== id)
      },
      onSubmit() {
        const formData = {
          email: this.email,
          age: this.age,
          password: this.password,
          confirmPassword: this.confirmPassword,
          country: this.country,
          hobbies: this.hobbyInputs.map(hobby => hobby.value),
          terms: this.terms
        };
        console.log(formData);
        axios.post('https://axios-vue-example.firebaseio.com/users.json', formData)
          .then(res => console.log(res))
          .catch(error => console.error(error));
      }
    }
  }
</script>
```

* `axios.post()` requires two args
  * The url: `https://axios-vue-example.firebaseio.com/users.json`
  * The data (`formData`)
    * Axios will automatically stringify this
  * Could pass a third arg, an object, where you can pass additional config requests
  * Chain a `.then()` method since it uses a promise
    * We can react to the result at some point in the future - that's why we use a promise
    * The argument to `then()` can be a function that will execute once the request is done
* Output when submitting a form
  * ![](images/2017-12-05-10-29-25.png)

## 22.5 - Sending a GET Request

`src/components/dashboard/dashboard.vue`

```html
<script>
  import axios from 'axios';

  export default {
    created() {
      axios.get('https://axios-vue-example.firebaseio.com/users.json')
        .then(res => console.log(res))
        .catch(error => console.error(error));
    }
  }
</script>
```

* When you `import axios`, it's the same instance as the one in `signup.vue`
  * So no need to worry about increasing bundle size
* `axios.get()` requires 1 arg
  * The url
  * Could pass second arg where you configure the request
* Output when you click the "Dashboard" button
  * ![](images/2017-12-05-10-34-19.png)

## 22.6 - Accessing and Using Response Data

`src/components/dashboard/dashboard.vue`

```html
<template>
  <div id="dashboard">
    <h1>That's the dashboard!</h1>
    <p>You should only get here if you're authenticated!</p>
    <p>Your email address: {{ email }}</p>
  </div>
</template>

<script>
  import axios from 'axios';

  export default {
    data() {
      return {
        email: ''
      }
    },
    created() {
      axios.get('https://axios-vue-example.firebaseio.com/users.json')
        .then(res => {
          console.log(res);
          const data = res.data;
          const users = [];
          for (let key in data) {
            const user = data[key];
            user.id = key;
            users.push(user);
          }
          console.log(users);
          this.email = users[0].email;
        })
        .catch(error => console.error(error));
    }
  }
</script>
```

* You only have access to the response in the `then()` function because this is async code
  * If there's any code after `axios.get()`, like `axios.get(); this.email = res.email;`, that code will execute immediately after `axios.get()` without waiting for a response
  * You only want to access data inside `then()`
* In order to get all the emails, you need to loop through all the keys (since every entry in firebase has a unique key)
* Output:
  * ![](images/2017-12-05-10-42-35.png)

## 22.7 - Setting a Global Request Configuration

`src/main.js`

```javascript
import Vue from 'vue'
import App from './App.vue'
import axios from 'axios';

import router from './router'
import store from './store'

axios.defaults.baseURL = 'https://axios-vue-example.firebaseio.com';
axios.defaults.headers.common['Authorization'] = 'laksdjflkja';
axios.defaults.headers.get['Accepts'] = 'application/json';
```

* By setting the base url here, you can have relative paths in all other calls
  * `axios.get('/users.json')`
  * `axios.post('/users.json', formData)`
* `axios.defaults.headers`
  * Axios will always set up default headers, but if you want to add additional ones, axios will merge the ones defined here
* Output in `Headers` tab in Chrome:
  * ![](images/2017-12-05-10-49-52.png)

## 22.8 - Using Interceptors

`src/main.js`

```javascript
axios.interceptors.request.use(config => {
  console.log('Request Interceptor', config);
  return config;
});

axios.interceptors.response.use(res => {
  console.log('Response Interceptor', res);
  return res;
});
```

* Interceptors get executed on every request or every response
* `use()`
  * Add new interceptor through this method
  * Takes a function as arg
  * You must at least return the configuration, otherwise you will block the request
* Output:
  * ![](images/2017-12-05-10-54-21.png)

`src/main.js`

```javascript
const reqInterceptor = axios.interceptors.request.use(config => {
  console.log('Request Interceptor', config);
  return config;
});

const resInterceptor = axios.interceptors.response.use(res => {
  console.log('Response Interceptor', res);
  return res;
});

axios.interceptors.request.eject(reqInterceptor);
axios.interceptors.request.eject(resInterceptor);
```

* To eject the interceptors, use `eject()`, which takes the id of the interceptor (returned by the `use()` function)

## 22.9 - Custom Axios Instances

* If you have an app where you want to target one set of URLs and not another, you can use custom instances

`src/axios-auth.js`

```javascript
import axios from 'axios';

const instance = axios.create({
  baseURL: 'https://axios-vue-example.firebaseio.com'
});

instance.defaults.headers.common['SOMETHING'] = 'something';

export default instance;
```

`src/components/auth/signup.vue`

```html
<script>
  import axios from '../../axios-auth';
...
```

* This will get the customized instance
* Output:
  * ![](images/2017-12-05-11-09-54.png)

## 22.10 - Resources and Links

* Axios: <https://github.com/axios/axios>