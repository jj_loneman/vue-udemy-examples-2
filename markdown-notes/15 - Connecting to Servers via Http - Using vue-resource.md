# 15 - Connecting to Servers via Http - Using vue-resource

## 15.1 - Introduction

* With AJAX calls, when you make a request to a server, you don't want to leave the page, you want to stay on the page and load parts of a page or some data

## 15.2 - Accessing Http via vue-resource

* You can use jQuery or another package
* `vue-resource` is another package made for this

### Setup

```commandline
npm install --save vue-resource
```

* `--save` ensures the package will be install as production dependency

`src/main.js`

```javascript
import Vue from 'vue'
import VueResource from 'vue-resource';
import App from './App.vue'

Vue.use(VueResource);

new Vue({
  el: '#app',
  render: h => h(App)
});
```

* Once you install `vue-resource`, you had to add it to the app to be able to use it
* `Vue.use()` tells Vue to add plugin to the core Vue functionality

## 15.3 - Creating an Application and Setting Up a Server (Firebase)

* Firebase is a service you can use as the backend for your web app or native app
  * It has database and authentication functionalities and much more
* Set `.read` and `.write` rules to `true` so we can access it freely (don't want to do that for production database)

```javascript
{
  "rules": {
    ".read": "true",
    ".write": "true"
  }
}
```

`src/App.vue`

```html
<template>
  <div class="container">
    <div class="row">
      <div class="col-xs-12 col-sm-8 col-sm-offset-2 col-md-6 col-md-offset-3">
        <h1>Http</h1>
        <div class="form-group">
          <label>Username</label>
          <input
              type="text"
              class="form-control"
              v-model="user.username"
          >
        </div>
        <div class="form-group">
          <label>Mail</label>
          <input
              type="text"
              class="form-control"
              v-model="user.email"
          >
        </div>
        <button
            class="btn btn-primary"
            @click="submit"
        >
          Submit
        </button>
      </div>
    </div>
  </div>
</template>

<script>
  export default {
    data() {
      return {
        user: {
          username: '',
          email: ''
        }
      };
    },
    methods: {
      submit() {
        console.log(this.user);
      }
    }
  }
</script>
```

* Output of clicking the Submit button:
  * ![](images/2017-12-05-03-46-38.png)

## 15.4 - Posting Data to a Server

`src/App.vue`

```html
<script>
  ...
    methods: {
      submit() {
        this.$http.post('https://vuejs-http-e134d.firebaseio.com/data.json', this.user)
          .then(response => {
            console.log(response);
          }, error => {
            console.error(error);
          });
      }
    }
  }
</script>
```

* Every Vue instance has access to `http` through `vue-resource` since we set it up gloablly in `main.js`
* The firebase url is `https://vuejs-http-e134d.firebaseio.com/data.json`
  * `data` is the node we want to post the data to
  * `data.json` because Firebase allows requests that target `.json` resources

* `this.$http.post()` method
  * The `vue-resource` package uses the Promise approach
  * Promises are good for async actions because it promises to give us data back in the future

* Output
  * ![](images/2017-12-05-03-55-41.png)
* Database
  * ![](images/2017-12-05-03-56-10.png)

## 15.5 - Getting and Transforming Data

`src/App.vue`

```html
<script>
  ...
    methods: {
      fetchData() {
        this.$http.get('https://vuejs-http-e134d.firebaseio.com/data.json')
          .then(response => {
            return response.json();
          })
          .then(data => console.log(data));
      }
    }
  }
</script>
```

* `response.json()` extracts the data in the GET request and wraps it in JSON
* Since the `get()` method first returns a promise (it won't always have data when it's first loaded), you have to chain another `.then()` method after the response is returned
* After clicking "Get Data"
  * ![](images/2017-12-05-04-04-33.png)

### New Code

`src/App.vue`

```html
<script>
  ...
    methods: {
      fetchData() {
        this.$http.get('https://vuejs-http-e134d.firebaseio.com/data.json')
          .then(response => {
            return response.json();
          })
          .then(data => {
            const resultArray = [];
            for (let key in data) {
              resultArray.push(data[key]);
            }
            this.users = resultArray;
          });
      }
    }
  }
</script>
```

* Vue is clever enough to update the DOM because we assigned a new value to `this.users` which is enough to trigger an update to the DOM
* After clicking "Get Data"
  * ![](images/2017-12-05-04-06-19.png)

## 15.6 - Configuring vue-resource Globally

`src/main.js`

```javascript
Vue.use(VueResource);

Vue.http.options.root = 'https://vuejs-http-e134d.firebaseio.com/data.json';
```

* Don't need `$http` except when you access it from within the instance. Since it's global just use `.http`
* The `root` is the root URL
* Can also set up `http.options.headers` or content-type globally

`src/App.vue`

```html
<script>
...
    methods: {
      submit() {
        this.$http.post('', this.user)
    ...
</script>
```

* If the root address is set up, then in the `post()` and `get()` functions, the first argument is what will be appended to the address

## 15.7 - Intercepting Requests

`src/main.js`

```javascript
Vue.http.interceptors.push((request, next) => {
  console.log(request);
  if (request.method === 'POST') {
    request.method = 'PUT';
  }
  next();
});
```

* You need `next` so that you can tell the method what to do when it's done
* In this case you'll see it did PUT instead
  * ![](images/2017-12-05-04-17-21.png)
* And in the database, the data is always updated
  * ![](images/2017-12-05-04-17-43.png)
  * No cryptic unique IDs
  * PUT always overwrites the old data

## 15.8 - Intercepting Responses

`src/main.js`

```javascript
Vue.http.interceptors.push((request, next) => {
  console.log(request);
  if (request.method === 'POST') {
    request.method = 'PUT';
  }
  next((response => {
    response.json = () => {
      return {
        messages: response.body
      };
    }
  }));
});
```

* Our code didn't work before because we didn't get the data as an array
* Now, our response is wrapped in a way that the app can process the data
* Note: You don't want to use response interceptor in production app globally in general since it'll modify every repsonse
* Output:
  * ![](images/2017-12-05-04-22-59.png)

## 15.9 - Where the "resource" in vue-resource Comes From

`src/main.js`

```javascript
Vue.http.options.root = 'https://vuejs-http-e134d.firebaseio.com/';
```

* Removed `data.json` at the end of the URL

`src/App.vue`

```html
<script>
  export default {
    data() {
      return {
        user: {
          username: '',
          email: ''
        },
        users: [],
        resource: {}
      };
    },
    methods: {
      submit() {
        // this.$http.post('data.json', this.user)
        //   .then(response => {
        //     console.log(response);
        //   }, error => {
        //     console.error(error);
        //   });
        this.resource.save({}, this.user);
      },
      fetchData() {
        this.$http.get('data.json')
          .then(response => {
            return response.json();
          })
          .then(data => {
            const resultArray = [];
            for (let key in data) {
              resultArray.push(data[key]);
            }
            this.users = resultArray;
          });
      }
    },
    created() {
      this.resource = this.$resource('data.json');
    }
  }
</script>
```

* `created()` is a good place to initialize resources in `App` component
* This does the same functionality as before in posting the data

## 15.10 - Creating Custom Resources

`src/App.vue`

```html
<script>
...
    methods: {
      submit() {
        this.resource.saveAlt(this.user);
      },
      fetchData() {
        this.$http.get('data.json')
          .then(response => {
            return response.json();
          })
          .then(data => {
            const resultArray = [];
            for (let key in data) {
              resultArray.push(data[key]);
            }
            this.users = resultArray;
          });
      }
    },
    created() {
      const customActions = {
        saveAlt: {
          method: 'POST',
          url: 'alternative.json'
        }
      };
      this.resource = this.$resource('data.json', {}, customActions);
    }
  }
</script>
```

* This is useful so you don't have to have duplicate code and enhances flexibility
* This posted to a new node called `alternative`:
  * ![](images/2017-12-05-04-32-46.png)

## 15.11 - Understanding Template URLs

`src/App.vue`

```html
<template>
...
        <input
            type="text"
            class="form-control"
            v-model="node"
        >
...
</template>
<script>
<script>
  export default {
    data() {
      return {
        user: {
          username: '',
          email: ''
        },
        users: [],
        resource: {},
        node: 'data'
      };
    },
    methods: {
      submit() {
        this.resource.saveAlt(this.user);
      },
      fetchData() {
        this.resource.getData({node: this.node})
          .then(response => {
            return response.json();
          })
          .then(data => {
            const resultArray = [];
            for (let key in data) {
              resultArray.push(data[key]);
            }
            this.users = resultArray;
          });
      }
    },
    created() {
      const customActions = {
        saveAlt: {
          method: 'POST',
          url: 'alternative.json'
        },
        getData: {
          method: 'GET',

        }
      };
      this.resource = this.$resource('{node}.json', {}, customActions);
    }
  }
</script>
```

* Here, added template url
* Database:
  * ![](images/2017-12-05-04-42-45.png)
* When `data` is in the input box and "Get Data" is clicked:
  * ![](images/2017-12-05-04-41-41.png)
* When `alternative` is in the input box and "Get Data" is clicked:
  * ![](images/2017-12-05-04-42-28.png)

## 15.12 - Resources and Links

* vue-resource on Github:
  * <https://github.com/yyx990803/vue-resource>
* Some Code Recipes for vue-resource:
  * <https://github.com/yyx990803/vue-resource/blob/master/docs/recipes.md>
* Template URLs:
  * <https://medialize.github.io/URI.js/uri-template.html>
* Requests and Responses (incl. Different File Formats): 
  * <https://github.com/yyx990803/vue-resource/blob/master/docs/http.md>