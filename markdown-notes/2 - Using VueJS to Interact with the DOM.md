# Section 2 - Using VueJS to Interact with the DOM

## 2.2 - Understanding VueJS Templates

`html`

```html
<script src="https://unpkg.com/vue/dist/vue.js"></script>

<div id="app">
  <p>{{ title }}</p>
</div>
```

`js`

```javascript
new Vue({
  el: '#app',
  data: {
    title: 'Hello World!'
  }
});
```

## 2.3 - How the VueJS Template Syntax and Instance Work Together

`html`

```html
<script src="https://unpkg.com/vue/dist/vue.js"></script>

<div id="app">
  <p>{{ sayHello() }}</p>
</div>
```

`js`

```javascript
new Vue({
  el: '#app',
  data: {
    title: 'Hello World!'
  },
  methods: {
    sayHello: function() {
      return 'Hello!';
    }
  }
});
```

## 2.4 - Accessing Data in the Vue Instance

`html`

```html
<script src="https://unpkg.com/vue/dist/vue.js"></script>

<div id="app">
  <p>{{ sayHello() }}</p>
</div>
```

`js`

```javascript
new Vue({
  el: '#app',
  data: {
    title: 'Hello World!'
  },
  methods: {
    sayHello: function() {
      //return title; (doesn't work)
      return this.title;
    }
  }
});
```

## 2.5 - Binding to Attributes

`html`

```html
<script src="https://unpkg.com/vue/dist/vue.js"></script>

<div id="app">
  <p>{{ sayHello() }} - <a href="{{ link }}">Google</a></p>
</div>
```

* This doesn't work - would try to take you to literally `http://{{ link }}` (encoded though)
* **Can only use {{ curly brace expansion }} in places where you'd normally use text; cannot use it on HTML attributes**

`js`

```javascript
new Vue({
  el: '#app',
  data: {
    title: 'Hello World!',
    link: 'http://google.com'
  },
  methods: {
    sayHello: function() {
      return this.title;
    }
  }
});
```

New Code:

`html`

```html
<script src="https://unpkg.com/vue/dist/vue.js"></script>

<div id="app">
  <p>{{ sayHello() }} - <a v-bind:href="link">Google</a></p>
</div>
```

* `v-bind` directive tells Vue to not use normal attribute, instead, bind it
  * `v-bind` takes an argument, the attribute you want to bind it with
    * e.g.) `v-bind:href`
  * Pass in the property you want to bind it with
    * e.g.) `v-bind:href="link"`
      * Where `link` is the property in the `data` attribute in our Vue object

## 2.7 - Disable Re-Rendering with `v-once`

`html`

```html
<script src="https://unpkg.com/vue/dist/vue.js"></script>

<div id="app">
  <h1>{{ title }}</h1>
  <p>{{ sayHello() }} - <a v-bind:href="link">Google</a></p>
</div>
```

`js`

```javascript
new Vue({
  el: '#app',
  data: {
    title: 'Hello World!',
    link: 'http://google.com'
  },
  methods: {
    sayHello: function() {
      this.title = 'Hello!';
      return this.title;
    }
  }
});
```

Output:

![](images/2017-10-10-14-09-29.png)

* All usages of `title` get re-rendered once the property changes
  * Because of this, both the `<h1>` and `<p>` tags will say `Hello!`, even though only `sayHello()` is modifying `title`
* `v-once`
  * Attribute added to an element which will only keep the initial value and not adjust when overwritten
  * e.g.) `<h1 v-once>{{ title }}</h1>`

New Code:

`html`

```html
<script src="https://unpkg.com/vue/dist/vue.js"></script>

<div id="app">
  <h1 v-once>{{ title }}</h1>
  <p>{{ sayHello() }} - <a v-bind:href="link">Google</a></p>
</div>
```

## 2.8 - How to Output Raw HTML

`html`

```html
<script src="https://unpkg.com/vue/dist/vue.js"></script>

<div id="app">
  <h1 v-once>{{ title }}</h1>
  <p>{{ sayHello() }} - <a v-bind:href="link">Google</a></p>
  <hr>
  <p>{{ finishedLink }}</p>
</div>
```

`js`

```javascript
new Vue({
  el: '#app',
  data: {
    title: 'Hello World!',
    link: 'http://google.com'
    finishedLink: '<a href="http://google.com">Google</a>'
  },
  methods: {
    sayHello: function() {
      this.title = 'Hello!';
      return this.title;
    }
  }
});
```

Output:

![](images/2017-10-10-14-18-44.png)

* By default, Vue doesn't render HTML as safety feature
* `v-html` attribute
  * Tells Vue to render the HTML and not escape it
  * Could be vulnerable to cross-site scripting attacks if it's code that the user created and executed

New Code:

`html`

```html
<script src="https://unpkg.com/vue/dist/vue.js"></script>

<div id="app">
  <h1 v-once>{{ title }}</h1>
  <p>{{ sayHello() }} - <a v-bind:href="link">Google</a></p>
  <hr>
  <p v-html>{{ finishedLink }}</p>
</div>
```

## 2.9 - Listening to Events

`html`

```html
<script src="https://unpkg.com/vue/dist/vue.js"></script>

<div id="app">
  <button v-on:click="increase">Click me</button>
  <p>{{ counter }}</p>
</div>
```

* `v-on` - listens to something to receive something from our template, an event
  * Takes argument of name of event we want to listen to (`click`, `mouseenter`, `mouseleave`, other default DOM events)

`js`

```javascript
new Vue({
  el: '#app',
  data: {
    counter: 0
  },
  methods: {
    increase: function() {
      this.counter++;
    }
  }
});
```

## 2.10 - Getting Event Data from the Event Object

`html`

```html
<script src="https://unpkg.com/vue/dist/vue.js"></script>

<div id="app">
  <button v-on:click="increase">Click me</button>
  <p>{{ counter }}</p>
  <p v-on:mousemove="updateCoordinates">Coordinates: {{ x }} / {{ y }}</p>
</div>
```

* `event` gets created and passed automatically through the `v-on` binding

`js`

```javascript
new Vue({
  el: '#app',
  data: {
    counter: 0
  },
  methods: {
    increase: function() {
      this.counter++;
    },
    updateCoordinates: function(event) {
      this.x = event.clientX;
      this.y = event.clientY;
    }
  }
});
```

## 2.11 - Passing your own Arguments with Events

`html`

```html
<script src="https://unpkg.com/vue/dist/vue.js"></script>

<div id="app">
  <button v-on:click="increase(2, $event)">Click me</button>
  <p>{{ counter }}</p>
  <p v-on:mousemove="updateCoordinates">Coordinates: {{ x }} / {{ y }}</p>
</div>
```

* Vue automatically fetches the default `event` argument and stores it in a variable with a protected name called `$event` (must be spelled exactly that)
  * Don't override it

`js`

```javascript
new Vue({
  el: '#app',
  data: {
    counter: 0
  },
  methods: {
    increase: function(step, event) {
      this.counter += 2;
    },
    updateCoordinates: function(event) {
      this.x = event.clientX;
      this.y = event.clientY;
    }
  }
});
```

![](images/2017-10-11-23-18-19.png)

## 2.12 - Modifying an Event - with Event Modifiers

`html`

```html
<script src="https://unpkg.com/vue/dist/vue.js"></script>

<div id="app">
  <button v-on:click="increase(2, $event)">Click me</button>
  <p>{{ counter }}</p>
  <p v-on:mousemove="updateCoordinates">
    Coordinates: {{ x }} / {{ y }}
    - <span v-on:mousemove="dummy">DEAD SPOT</span>
  </p>
</div>
```

`js`

```javascript
new Vue({
  el: '#app',
  data: {
    counter: 0
  },
  methods: {
    increase: function(step, event) {
      this.counter += 2;
    },
    updateCoordinates: function(event) {
      this.x = event.clientX;
      this.y = event.clientY;
    },
    dummy: function(event) {
      event.stopPropagation();
    }
  }
});
```

* `event.stopPropagation()` means that the event will not propagate up to elements containing the element (the `<span>` in this case)
* When hovering over the `<span>` `DEAD SPOT`, the coords won't get updated
  ![](images/2017-10-11-23-32-15.png)

New code that's easier:

`html`

```html
<script src="https://unpkg.com/vue/dist/vue.js"></script>

<div id="app">
  <button v-on:click="increase(2, $event)">Click me</button>
  <p>{{ counter }}</p>
  <p v-on:mousemove="updateCoordinates">
    Coordinates: {{ x }} / {{ y }}
    - <span v-on:mousemove.stop="">DEAD SPOT</span>
  </p>
</div>
```

`js`

```javascript
new Vue({
  el: '#app',
  data: {
    counter: 0
  },
  methods: {
    increase: function(step, event) {
      this.counter += 2;
    },
    updateCoordinates: function(event) {
      this.x = event.clientX;
      this.y = event.clientY;
    }
  }
});
```

## 2.13 - Modifying an Event - with Key Modifiers

`html`

```html
<script src="https://unpkg.com/vue/dist/vue.js"></script>

<div id="app">
  <button v-on:click="increase(2, $event)">Click me</button>
  <p>{{ counter }}</p>
  <p v-on:mousemove="updateCoordinates">
    Coordinates: {{ x }} / {{ y }}
    - <span v-on:mousemove.stop="">DEAD SPOT</span>
  </p>
  <input type="text" v-on:keyup="alertme">
</div>
```

* This does the JS alert on every time the key is let up
* Can add keys for modifiers: `v-on:keyup.enter`
* Can also do chaining: `v-on:keyup.enter.space`
  * Whenever space or enter is pressed

`js`

```javascript
new Vue({
  el: '#app',
  data: {
    counter: 0
  },
  methods: {
    increase: function(step, event) {
      this.counter += 2;
    },
    updateCoordinates: function(event) {
      this.x = event.clientX;
      this.y = event.clientY;
    },
    alertMe: function() {
      alert("Alert!");
    }
  }
});
```

## 2.14 - Template JS Expressions

`html`

```html
<script src="https://unpkg.com/vue/dist/vue.js"></script>

<div id="app">
  <button v-on:click="increase(2, $event)">Click me</button>
  <button v-on:click="counter++">Click me</button>
  <p>{{ counter }}</p>
  <p v-on:mousemove="updateCoordinates">
    Coordinates: {{ x }} / {{ y }}
    - <span v-on:mousemove.stop="">DEAD SPOT</span>
  </p>
  <input type="text" v-on:keyup="alertme">
</div>
```

* Wherever you can access your Vue instance, you can write any valid Javascript as long as:
  * Is only one expression, no if statement, for loop, etc.
  * Can do ternary expressions
* This code would increment counter by one when click 2nd button

If changed to this:

`html`

```html
<script src="https://unpkg.com/vue/dist/vue.js"></script>

<div id="app">
  <button v-on:click="increase(2, $event)">Click me</button>
  <button v-on:click="counter++">Click me</button>
  <p>{{ counter * 2 }}</p>
  <p v-on:mousemove="updateCoordinates">
    Coordinates: {{ x }} / {{ y }}
    - <span v-on:mousemove.stop="">DEAD SPOT</span>
  </p>
  <input type="text" v-on:keyup="alertme">
</div>
```

* Would increase step by 4 each button click for first button

## 2.15 - DOM Interaction - Two-Way Data Binding

`html`

```html
<script src="https://unpkg.com/vue/dist/vue.js"></script>

<div id="app">
  <input type="text" v-model="name">
  <p>{{ name }}</p>
</div>
```

* Want to populate input field with name of user
* Whenever change input field - change the name property
* Can do two events at the same time (listening to events and performing events)

`js`

```javascript
new Vue({
  el: '#app',
  data: {
    name: 'Max'
  }
});
```

![](images/2017-10-19-08-15-43.png)

## 2.16 - Reacting to Changes with Computed Properties

`html`

```html
<script src="https://unpkg.com/vue/dist/vue.js"></script>

<div id="app">
  <button v-on:click="increase">Increase</button>
  <button v-on:click="decrease">Increase</button>
  <p>Counter: {{ counter }}</p>
  <p>Result: {{ result }}</p>
</div>
```

`js`

```javascript
new Vue({
  el: '#app',
  data: {
    counter: 0,
    result: ''
  },
  methods: {
    increase: function() {
      this.counter++;
      this.result = this.counter > 5 ? 'Greater 5' : 'Smaller 5';
    }
    decrease: function() {
      this.counter--;
      this.result = this.counter > 5 ? 'Greater 5' : 'Smaller 5';
    }
  }
});
```

![](images/2017-10-19-20-34-03.png)

* Hard to maintain this code b/c the `this.result =` function is duplicated in both `increase()` and `decrease()`

New Code:

`html`

```html
<script src="https://unpkg.com/vue/dist/vue.js"></script>

<div id="app">
  <button v-on:click="counter++">Increase</button>
  <button v-on:click="counter--">Increase</button>
  <button v-on:click="secondCounter++">Increase Second</button>
  <p>Counter: {{ counter }} | {{ secondCounter }}</p>
  <p>Result: {{ result() }} | {{ output }}</p>
</div>
```

* Even though `secondCounter` is not defined in the `data` object, it still gets created as a variable and can be used
* `result()` is called with parens whereas `output` isn't

`js`

```javascript
new Vue({
  el: '#app',
  data: {
    counter: 0
  },
  computed: {
    output: function() {
      return this.counter > 5 ? 'Greater 5' : 'Smaller 5';
    }
  },
  methods: {
    result() {
      return this.counter > 5 ? 'Greater 5' : 'Smaller 5';
    }
  }
});
```

* Can't do something like `result: this.counter > 5 ? ...` b/c data isn't reactive
* Vue updates the page whenever it needs to (like whenever a data property changes)
* It doesn't know if the `result()` method needs to be called, so it re-executes the function every time
* `computed` only executes when it needs to
  * Vue analyzes the code and checks to see if the variable used in the computed function needs to be updated. 
* When `secondCounter++` happens, the `output()` function in `computed` is not executed, but `result()` is, even though it doesn't need to
* **Only use the `method` way when you know it needs to be updated on every refresh of the webpage**
* `computed` properties cache variables - optimized

![](images/2017-10-19-20-46-25.png)

## 2.17 - An Alternative to Computed Properties: Watching for Changes (Interaction Watch)

`html`

```html
<script src="https://unpkg.com/vue/dist/vue.js"></script>

<div id="app">
  <button v-on:click="counter++">Increase</button>
  <button v-on:click="counter--">Increase</button>
  <p>Counter: {{ counter }}</p>
  <p>Result: {{ result }}</p>
</div>
```

`js`

```javascript
new Vue({
  el: '#app',
  data: {
    counter: 0
  },
  computed: {
    output: function() {
      return this.counter > 5 ? 'Greater 5' : 'Smaller 5';
    }
  },
  watch: {
    counter: function(value) {
      var vm = this;
      setTimeout(function() {
        vm.counter = 0;
      }, 2000);
    }
  },
  methods: {
    result() {
      return this.counter > 5 ? 'Greater 5' : 'Smaller 5';
    }
  }
});
```

* For `watch`, set up the property name you want to watch
  * Like `counter` (must match one of the properties)
  * Then, as function, specify code want to execute whenever the counter changes
    * Vue passes the value changed to the function to `value`
* Best practice to use `computed` whenever you can
  * Allows Vue to run more optimally
  * Some cases where `computed` properties can't be used
    * If you need asynchronous tasks to be run
    * `computed` properties always need to be run synchronously
      * In the example in `output`, the return needs to happen immediately - no way can reach out to server to get another value
* In `counter` watch, because the function passed to `setTimeout()` is a closure, you must store the Vue instance in a separate variable
  * `this` isn't available in callback closure

![](images/2017-10-19-21-00-29.png)

* When Increase is clicked, the first number will increase and reset after 2 seconds

## 2.18 - Shorthands

`html`

```html
<script src="https://unpkg.com/vue/dist/vue.js"></script>

<div id="app">
  <button @click="changeLink">Click to Change Link</button>
  <a :href="link">Link</a>
</div>
```

* `@click` == `v-on:`
  * Basically saying "at click, do this"
* `:` == `v-bind:`

`js`

```javascript
new Vue({
  el: '#app',
  data: {
    link: 'http://google.com'
  },
  methods: {
    changeLink: function() {
      this.link = 'http://apple.com';
    }
  }
});
```

![](images/2017-10-19-21-15-27.png)

## 2.19 - Dynamic Styling with CSS Classes - Basic

`html`

```html
<script src="https://unpkg.com/vue/dist/vue.js"></script>

<div id="app">
  <div 
    class="demo" 
    @click="attachRed = !attachRed"
    :class="{red: attachRed}"
  ></div>
  <div class="demo" :class="{red: attachRed}"></div>
  <div class="demo"></div>
</div>
```

* `@click` will toggle `attachRed` `true` and `false`
* `v-bind` can take object with name of css class as key and `true` or `false` value
* If the css class had a special character in it (a dash), then you need to put the key in a string (`red-class`)
* Putting the same `{red: attachRed}` in the second div would toggle both squares at the same time since they both use the same property

`css`

```css
.demo {
  width: 100px;
  height: 100px;
  background-color: gray;
  display: inline-block;
  margin: 10px;
}

.red {
  background-color: red;
}
.green {
  background-color: green;
}
.blue {
  background-color: blue;
}
```

`js`

```javascript
new Vue({
  el: '#app',
  data: {
    attachRed: false
  }
});
```

![](images/2017-10-19-21-23-55.png)

## 2.20 - CSS Classes - Object in Data

`html`

```html
<script src="https://unpkg.com/vue/dist/vue.js"></script>

<div id="app">
  <div
    class="demo"
    @click="attachRed = !attachRed"
    :class="{red: attachRed}"
  ></div>
  <div
    class="demo"
    :class="{red: attachRed, blue: !attachRed}"
  ></div>
  <div class="demo"></div>
</div>
```

* By adding `blue` key, you can toggle between just setting the first square to blue and both to red
  ![](images/2017-10-19-21-26-47.png)
* However, the object can grow rather large

New Code:

`html`

```html
<script src="https://unpkg.com/vue/dist/vue.js"></script>

<div id="app">
  <div
    class="demo"
    @click="attachRed = !attachRed"
    :class="divClasses"
  ></div>
  <div
    class="demo"
    :class="divClasses"
  ></div>
  <div class="demo"></div>
</div>
```

* Don't need the parens for `divClasses`

`js`

```javascript
new Vue({
  el: '#app',
  data: {
    attachRed: false
  },
  computed: {
    divClasses: function() {
      return {
        red: this.attachRed,
        blue: !this.attachRed
      };
    }
  }
});
```

* Since `divClasses` depends on the `attachRed` property, it needs to be `computed`
* Works exactly the same as before, but the data handling is outsourced in a computed property

## 2.21 - Dynamic Styling with CSS Classes - Using Class Names

`html`

```html
<script src="https://unpkg.com/vue/dist/vue.js"></script>

<div id="app">
  <div
    class="demo"
    @click="attachRed = !attachRed"
    :class="{red: attachRed}"
  ></div>
  <div class="demo" :class="divClasses"
  ></div>
  <div class="demo" :class="color"></div>
  <hr>
  <input type="text" v-model="color">
</div>
```

`js`

```javascript
new Vue({
  el: '#app',
  data: {
    attachRed: false,
    color: 'green'
  },
  computed: {
    divClasses: function() {
      return {
        red: this.attachRed,
        blue: !this.attachRed
      };
    }
  }
});
```

* The input field is set to the color of the css class
  ![](images/2017-10-19-21-35-49.png)
* Could change the text to `red` or whatever
  ![](images/2017-10-19-21-37-08.png)

If we change the code to this:

`html`

```html
<script src="https://unpkg.com/vue/dist/vue.js"></script>

<div id="app">
  <div
    class="demo"
    @click="attachRed = !attachRed"
    :class="{red: attachRed}"
  ></div>
  <div class="demo" :class="divClasses"
  ></div>
  <div class="demo" :class="[color, {red: attachRed}]"></div>
  <hr>
  <input type="text" v-model="color">
</div>
```

* You can attach multiple classes using array syntax
* You can add the object `{red: attachRed}` as a second argument
* Vue will analyze the array of classes and merge it all into one list of css classes based on whatever the items in the array resolve to
  * Resolve to `color` property and the object
* Output of code when remove class from input field and click on one of the squares
  ![](images/2017-10-19-21-42-01.png)

## 2.22 - Ssetting Styles Dynamically (Without CSS Classes)

`html`

```html
<script src="https://unpkg.com/vue/dist/vue.js"></script>

<div id="app">
  <div class="demo" :style="{backgroundColor: color}"></div>
  <div class="demo"></div>
  <div class="demo"></div>
  <hr>
  <input type="text" v-model="color">
</div>
```

* Use camelCasing for css attributes - it will resolve to whatever it is, so `backgroundColor` will resolve to `background-color`
  * If you want to use `background-color` as the key in the `style` attribute, you would need to enclose the key in quotes, as `-` is a special character

`js`

```javascript
new Vue({
  el: '#app',
  data: {
    color: 'gray'
  }
});
```

`css`

```css
.demo {
  width: 100px;
  height: 100px;
  background-color: gray;
  display: inline-block;
  margin: 10px;
}
```

![](images/2017-11-08-21-50-21.png)

* Whenever you type in the class name in the input field, it will update the background color of the square

### New Code

`html`

```html
<script src="https://unpkg.com/vue/dist/vue.js"></script>

<div id="app">
  <div class="demo" :style="{backgroundColor: color}"></div>
  <div class="demo" :style="myStyle"></div>
  <div class="demo"></div>
  <hr>
  <input type="text" v-model="color">
  <input type="text" v-model="width">
</div>
```

* The second `div` just shows how we can manipulate the style in a computed function

`js`

```javascript
new Vue({
  el: '#app',
  data: {
    color: 'gray',
    width: 100
  },
  computed: {
    myStyle: function() {
      return {
        backgroundColor: this.color,
        width: this.width + 'px'
      };
    }
  }
});
```

* **camelCasing is important**

![](images/2017-11-08-21-54-26.png)

* Allows user to type in color and width

## 2.23 - Styling Elements with Array Syntax

`html`

```html
<script src="https://unpkg.com/vue/dist/vue.js"></script>

<div id="app">
  <div class="demo" :style="{backgroundColor: color}"></div>
  <div class="demo" :style="myStyle"></div>
  <div class="demo" :style="[myStyle, {height: width + 'px'}]"></div>
  <hr>
  <input type="text" v-model="color">
  <input type="text" v-model="width">
</div>
```

* You can combine multiple styles using the array syntax

![](images/2017-11-08-21-57-24.png)
