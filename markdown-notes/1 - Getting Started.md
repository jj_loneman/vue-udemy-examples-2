# Section 1 - Getting Started - VueJS

## 1.1 - Overview

* Allows you to create small JS widgets
* Re-render various parts of a webpage to make it reactive
* Can create single-page applications
* Don't need to reach out to any servers to wait for anything to load
* Why not choose another fw like Angular?
  * Vue lean and small regarding file size
  * 16kb minified and gzipped for core fw (w/o router fw)
  * Fast at runtime (beats Angular 2 and React)

## 1.2 - Creating First App

`index.js`

```javascript
new Vue({
  el: '#app',
  data: {
    title: 'Hello World!'
  },
  methods: {
    changeTitle: function(event) {
      this.title = event.target.value;
    }
  }
});
```

* Through `el` property, takes string as value, set up which part of HTML code should be under control of this Vue instance
* `this` refers to data object - some magic done in the background by Vue, proxies all data properties (like `title`) to top Vue instance
* We have access to all properties stored in data, methods with `this`
* Putting `event` into `function` param is passed to method automatically by Vue

`index.html`

```html
<script src="https://cdn.jsdelivr.net/npm/vue"></script>

<div id="app">
  <input type="text" v-on:input="changeTitle">
  <p>{{title}}</p>
</div>
```

* Directive
  * `v-on` tells Vue to listen to some event (passed into the directive)

## 1.4 - Course Structure

1. Getting Started
1. Interacting with the DOM (templates)
1. Understanding the VueJS instance
1. Vue CLI
1. Components
1. Forms
1. Directives, Filters & Mixins
1. Animations & Transitions
1. Working with HTTP
1. Routing
  a. SPA
1. State management
  a. SPA
1. Deploying a Vue app

## 1.5 - Course Content

1. Projects
  a. 1st Project - basics, template interaction
  a. 2nd Project - components
  a. 3rd Project - animations
  a. 4th Project - final project
    i. Routing, state management
1. Quick exercises
1. Source code attached to last lecture of each course module
1. Ask and answer questions in Q&A