# 5 - Understanding the VueJS Instance

## 5.1 - VueJS Instance Basics

`html`

```html
<script src="https://unpkg.com/vue/dist/vue.js"></script>

<div id="app">
  <h1 ref="heading">{{ title }}</h1>
  <button v-on:click="show">Show Paragraph</button>
  <p v-if="showParagraph">This is not always visible</p>
</div>
```

`js`

```javascript
new Vue({
  el: '#app',
  data: {
    title: 'The VueJS Instance',
    showParagraph: false
  },
  methods: {
    show: function() {
      this.showParagraph = true;
      this.updateTitle('The VueJS Instance (Updated)');
    },
    updateTitle: function(title) {
      this.title = title;
    }
  },
  computed: {
    lowercaseTitle: function() {
      return this.title.toLowerCase();
    }
  },
  watch: {
    title: function(value) {
      alert('Title changed, new value: ' + value);
    }
  }
});
```

* The VueJS instance is the middleman between DOM and business logic
  * We pack all business logic into Vue instance
    * Either in data property or method call

![](images/2017-11-09-01-25-27.png)

## 5.2 - Using Multiple Vue Instances

`html`

```html
<script src="https://unpkg.com/vue/dist/vue.js"></script>

<div id="app1">
  <h1>{{ title }}</h1>
</div>

<div id="app2">
  <h1>{{ title }}</h1>
</div>
```

`js`

```javascript
new Vue({
  el: '#app1',
  data: {
    title: 'The VueJS Instance'
  });

new Vue({
  el: '#app2',
  data: {
    title: 'The Second VueJS Instance'
  });
```

* Note: When we use `this`, can only refer to that instance, can't reference other instances

![](images/2017-11-09-01-29-49.png)

## 5.3 - Accessing the Vue Instance from Outside

`html`

```html
<script src="https://unpkg.com/vue/dist/vue.js"></script>

<div id="app1">
  <h1 ref="heading">{{ title }}</h1>
  <button v-on:click="show">Show Paragraph</button>
  <p v-if="showParagraph">This is not always visible</p>
</div>

<div id="app2">
  <h1>{{ title }}</h1>
  <button @click="onChange">Change something in app1
</div>
```

`js`

```javascript
var vm1 = new Vue({
  el: '#app1',
  data: {
    title: 'The VueJS Instance',
    showParagraph: false
  },
  methods: {
    show: function() {
      this.showParagraph = true;
      this.updateTitle('The VueJS Instance (Updated)');
    },
    updateTitle: function(title) {
      this.title = title;
    }
  },
  computed: {
    lowercaseTitle: function() {
      return this.title.toLowerCase();
    }
  },
  watch: {
    title: function(value) {
      alert('Title changed, new value: ' + value);
    }
  }
});

var vm2 = new Vue({
  el: '#app2',
  data: {
    title: 'The second Instance'
  },
  methods: {
    onChange: function() {
      vm1.title = 'Changed!';
    }
  }
})
```

* The variable name `vm` stands for view-model, since VueJS represents view-model of the app

![](images/2017-11-09-01-34-45.png)

* We could write this code in the JS before our `vm2` declaration:

```javascript
setTimeout(function() {
  vm1.title = 'Changed by Timer';
}, 3000);
```

* Vue proxies the attributes of the Vue instance to the variable

![](images/2017-11-09-01-36-20.png)

## 5.4 - How VueJS Manages Your Data and Methods

* In theory, we're not creating the Vue instance
  * It's an object shipped with the f/w, we don't know what properties it has
  * Everything we pass to the Vue object, we pass to the constructor
* Vue takes the data props and methods and uses them as native properties on the Vue instance itself
* Sets up a sort of watcher (watcher layer) where it recognizes when something happens
* You can't add new properties and use them in any Vue-like way, such as doing something like:
  ```javascript
  vm1.someProp = 'Hello!';
  ```
  * There won't be a proxied getter/setter for that property

## 5.5 - A Closer Look at $el and $data

```javascript
console.log(vm1.$data);
```

* Accesses `vm1` data block

## 5.6 - Placing $refs and Using Them on Your Templates

`html`

```html
<script src="https://unpkg.com/vue/dist/vue.js"></script>

<div id="app1">
  <h1 ref="heading">{{ title }}</h1>
  <button v-on:click="show">Show Paragraph</button>
  <p v-if="showParagraph">This is not always visible</p>
</div>

<div id="app2">
  <h1>{{ title }}</h1>
  <button @click="onChange">Change something in app1
</div>
```

```javascript
vm1.$refs.heading.innerText = 'Something else';
```

* `ref` is a Vue-specific attribute, not a native HTML attribute
* `$refs` can be accessed inside or outside the instance
* VueJS takes the code - creates a template based on the HTML
  * If we access an element like we do here, we change this directly in the DOM, but we're not changing the template of Vue
  * Therefore, whenever Vue re-renders the DOM for us, it takes the whole template, as it always does, and then re-renders the DOM based on this template
    * So it overrides the change we made here
* We are able to interact with HTML elements with the `ref` key and `$refs`
  * But if we change something there, it's not reactive
  * Not part of VueJS's instance - it's directly in the DOM
* It may be convenient to use a `ref` reference we need to get the value of something or access a native element
  * Easier than using a query selector

## 5.7 - Vue API

[](vuejs.org/api)

## 5.8 - Mounting a Template

* If we delete the `el` property in the Vue instance and instead do:
  ```js
  vm1.$mount('#app1');
  ```
  * It does the same thing as the `el` property
  * `el` property makes it easier for us
    * If we know where to mount it, we should use `el`, as that's what it's meant for
    * Same as `$mount`, but automatic
  * You would use `$mount` if you want to mount it later

### Templates

`html`

```html
<div id="app3"></div>
```

`js`

```javascript
var vm3 = new Vue({
  template: '<h1>Hello!</h1>'
});

vm3.$mount('#app3');
```

![](images/2017-11-09-02-06-08.png)

## 5.9 - Components

`js`

```javascript
var vm3 = new Vue({
  el: 'hello',
  template: '<h1>Hello!</h1>'
});

vm3.$mount('#app3');
```

`html`

```html
<hello></hello>
<hello></hello>
```

* This would only replace the first `hello` tag

### New Code

```html
<div id="app2">
  <hello></hello>
  <hello></hello>
</div>
```

`js`

```javascript
Vue.component('hello', {
  template: '<h1>Hello!</h1>'
});
```

* This code does replace both instances of `<hello>`
* Can access it anywhere in the application
* It just replaces `<hello>` element with the template

## 5.10 - Limitations of Some Templates

* Two versions of Vue:
  * Vue with compiler built-in
    * Runs in browser
    * Takes template and re-compiles it in browser in DOM
  * W/o compiler
    * We have to compile templates during build process
    * When ship app, only have compiled JS code which can get executed whenever JS needs to re-render

## 5.11 - How Vue Updates the DOM

* Each property we set up has its own watcher
  * Constructor creates its own watcher
  * Doesn't update everything in the real DOM
  * Accessing DOM is super slow
* Extra layer - virtual layer
  * Very very quick to access
  * Vue watches for changes, writes to virtual DOM
    * Recreates virtual DOM
    * Then takes difference b/t template and virtual DOM and updates real DOM

![](images/2017-11-09-02-33-53.png)

![](images/2017-11-09-02-34-09.png)

## 5.12 - VueJS Instance Lifecycle

* Lifecycle instance starts with `new Vue()` constructor
  * Then, execute first lifecycle method `beforeCreate()`
    * Before the instance is created
    * Initializes data and events that we pass to the Vue object
* Then creates instance and calls `created()` method
  * Compiles template or derives template from HTML code if we use `el` or `$mount`
  * Either uses template from `template` property or the one it derives
* Then, `beforeMount()` is called
  * Called right before template is mounted to real DOM
  * Replaces `el` with compiled template
  * Still not mounted, but now Vue inserted all the values for string interpolations, set up all the bindings, etc.
    * Converted it to real HTML behind the scenes
* Then mount to DOM
* Not done, we have ongoing lifecyle we can tap into multiple times
  * If some data changes and DOM is re-rendered
  * Get the `beforeUpdate()` lifecycle hook right before we re-render the part of the DOM
  * Then calls `updated()` method after DOM is re-rendered
* Also have `beforeDestroy()` method right before instance is destroyed
* When instance is destroyed, `destroyed()` is called

![](images/2017-11-09-02-41-51.png)

## 5.13 - Vue Instance Lifecycle in Practice

`html`

```html
<div id="app">
  <h1>{{ title }}</h1>
  <button @click="title = 'changed'">Update Title</button>
  <button @click="destroy">Destroy</button>
</div>
```

`js`

```javascript
new Vue({
  el: '#app',
  data: {
    title: 'The VueJS instance'
  },
  beforeCreate: function() {
    console.log('beforeCreate()');
  },
  created: function() {
    console.log('created()');
  },
  beforeMount: function() {
    console.log('beforeMount()');
  },
  mounted: function() {
    console.log('mounted()');
  },
  beforeUpdate: function() {
    console.log('beforeUpdate()');
  },
  updated: function() {
    console.log('updated()');
  },
  beforeDestroy: function() {
    console.log('beforeDestroy()');
  },
  destroyed: function() {
    console.log('destroyed()');
  },
  methods: {
    destroy: function() {
      this.$destroy();
    }
  }
});
```

* On app start:
  ![](images/2017-11-09-02-47-44.png)
* After clicking `Update Title`:
  ![](images/2017-11-09-02-48-27.png)
  * We updated a property which required the DOM to be changed
  * When you click the button again, it sets the `title` property to `Changed` again, but it doesn't re-render the DOM because nothing was really changed
* After clicking `Destroy`:
  ![](images/2017-11-09-02-50-03.png)
  * Nothing is connected to Vue anymore, buttons won't work, it destroys all connections and JS logic
