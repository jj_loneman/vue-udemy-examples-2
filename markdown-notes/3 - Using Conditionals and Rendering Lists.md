# 3 - Using Conditionals and Rendering Lists

## 3.1 - Conditional Rendering with `v-if`

`html`

```html
<script src="https://unpkg.com/vue/dist/vue.js"></script>

<div id="app">
  <p v-if="show">You can see me!</p>
  <p v-else>Now you see me!</p>
  <p>Do you also see me?</p>
  <button @click="show = !show">Switch</button>
</div>
```

`js`

```javascript
new Vue({
  el: '#app',
  data: {
    show: true
  }
});
```

* Actually removes or adds the element from or to the DOM
* The argument to `v-if` takes a boolean value

![](images/2017-11-09-02-58-52.png)

* The element gets replaced by an HTML comment
* If we add a `v-else`, then that element displays if the `v-if` element is false
  * There is no `v-elseif` directive, `v-else` is just if you know you have an either-or element
  * ![](images/2017-11-09-03-02-40.png)
* `v-if` also removes all nested elements inside it if it's false

## 3.2 - `v-else-if` in Vue 2.1

* Actually, Vue 2.1+ now gives access to the `v-else-if` directive
* [](https://github.com/vuejs/vue/releases?after=v2.1.5)

`html`

```html
<div v-if="type === 'a'">A</div>
<div v-else-if="type === 'b'">B</div>
<div v-else>C</div>
```

## 3.3 - Using an Alternative `v-if` syntax

`html`

```html
<script src="https://unpkg.com/vue/dist/vue.js"></script>

<div id="app">
  <p v-if="show">You can see me!</p>
  <p v-else>Now you see me!</p>
  <template v-if="show">
    <h1>Heading</h1>
    <p>Inside a template</p>
  </template>
  <p>Do you also see me?</p>
  <button @click="show = !show">Switch</button>
</div>
```

* `<template>` tag doesn't get rendered in the DOM
  * Is valid HTML5 code
  * Can use it to group elements together in parallel

`js`

```javascript
new Vue({
  el: '#app',
  data: {
    show: true
  }
});
```

* Example of `<template>` tag not actually being present in DOM
  ![](images/2017-11-09-03-11-34.png)

## 3.4 - Don't Detach with `v-show`

`html`

```html
<script src="https://unpkg.com/vue/dist/vue.js"></script>

<div id="app">
  <p v-if="show">You can see me!</p>
  <p v-else>Now you see me!</p>
  <template v-if="show">
    <h1>Heading</h1>
    <p>Inside a template</p>
  </template>
  <p v-show="show">Do you also see me?</p>
  <button @click="show = !show">Switch</button>
</div>
```

* Just adds a `display: none` to the element style
  ![](images/2017-11-09-03-13-58.png)
* Better performance to use `v-if` because removing elements makes going through the DOM more quickly

## 3.5 - Rendering Lists with `v-for`

`html`

```html
<script src="https://unpkg.com/vue/dist/vue.js"></script>

<div id="app">
  <ul>
    <li v-for="ingredient in ingredients">{{ ingredient }}</li>
  </ul>
</div>
```

`js`

```javascript
new Vue({
  el: '#app',
  data: {
    ingredients: ['meat', 'fruit', 'cookies'],
    persons: [
      {name: 'Max', age: 27, color: 'red'},
      {name: 'Anna', age: 'unknown', color: 'blue'}
    ]
  }
});
```

![](images/2017-11-09-03-18-24.png)

## 3.6 - Getting the Current Index

`html`

```html
<script src="https://unpkg.com/vue/dist/vue.js"></script>

<div id="app">
  <ul>
    <li v-for="(ingredient, i) in ingredients">{{ ingredient }} ({{ i }})</li>
  </ul>
</div>
```

* This format `(currentElement, index)` entirely depends on order

![](images/2017-11-09-03-20-10.png)

## 3.7 - Using an Alternative `v-for` Syntax

`html`

```html
<script src="https://unpkg.com/vue/dist/vue.js"></script>

<div id="app">
  <ul>
    <li v-for="(ingredient, i) in ingredients">{{ ingredient }} ({{ i }})</li>
  </ul>
  <template v-for="(ingredient, index) in ingredients">
    <h1>{{ ingredient }}</h1>
    <p>{{ index }}</p>
  </template>
</div>
```

![](images/2017-11-09-03-33-34.png)

* It's a way to iterate through multiple non-nested elements

## 3.8 - Looping Through Objects

`html`

```html
<script src="https://unpkg.com/vue/dist/vue.js"></script>

<div id="app">
  <ul>
    <li v-for="(ingredient, i) in ingredients">{{ ingredient }} ({{ i }})</li>
  </ul>
  <hr>
  <ul>
    <li v-for="person in persons">{{ person }}
      <span v-for="value in person">{{ value }}</span>
    </li>
  </ul>
  <hr>
  <template v-for="(ingredient, index) in ingredients">
    <h1>{{ ingredient }}</h1>
    <p>{{ index }}</p>
  </template>
</div>
```

![](images/2017-11-09-03-36-56.png)

* If we change the syntax to:
  ```html
  <div v-for="(value, key, index) in person">{{ key }} : {{ value }} ({{ index }})</div>
  ```
  ![](images/2017-11-09-03-38-10.png)

## 3.9 - Looping Through a List of Numbers

```html
<span v-for="n in 10">{{ n }}</span>
```

* Vue loops from 1-10:
  ![](images/2017-11-09-03-39-19.png)

## 3.10 - Keeping Track of Elements When Using `v-for`

```html
<script src="https://unpkg.com/vue/dist/vue.js"></script>

<div id="app">
  <ul>
    <li v-for="(ingredient, i) in ingredients">{{ ingredient }} ({{ i }})</li>
  </ul>
  <button @click="ingredients.push('spices')">Add New</button>
  <hr>
  <ul>
    <li v-for="person in persons">{{ person }}
      <div v-for="(value, key, index) in person">{{ key }} : {{ value }} ({{ index }})</div></span>
    </li>
  </ul>
  <hr>
  <template v-for="(ingredient, index) in ingredients">
    <h1>{{ ingredient }}</h1>
    <p>{{ index }}</p>
  </template>
</div>
```

![](images/2017-11-09-03-41-41.png)

* If Vue needs to update one of the values in the array b/c somewhere in the code, you change one of the elements
* When you click `Add new`, it will add two `spices` objects to the array
* Vue proxies the `push()` method here, because it doesn't create a new array, it just adds items to the existing one
  * That's a bit hard to track since object itself - the array - doesn't change here since it's a reference type
  * The pointer to the type hasn't changed, only the value in memory
    * But for that to be realized, you have to watch the value in memory, which Vue does automatically
    * This is why we get the expected behavior
* How does Vue update the list if some item needs to be changed?
  * It updates the position in the array where something changed
  * If you were to override the second element through two-way data binding, it would update the second element in the list
  * It doesn't keep track of specific element it create
  * Only patch it in the second position
  * Often times, this is the behavior you want
  * But, if you want to be really safe and make sure Vue knows about the position and the actual list item, you need to assign unique key to that list item.

```html
<li v-for="(ingredient, i) in ingredients" :key="ingredient">{{ ingredient }} ({{ i }})</li>
```

* You might pass the index to `:key`
  * That's kind of tricky, as that index is derived from the list itself and set dynamically while rendering the list
* A better key is a real unique value
  * Since each ingredient is unique, you can use it as a unique key
* When running the code, works same as before, but:
  * Behind the scenes, you're safe, as Vue stores position of element and the element itself
  * When it reorders it, it will take the actual element and reorder it
    * Not just override the values in some of the positions it finds in the array

* **If you run into any strange side-effects that some items are updated in a different place than you expect them to be**:
  * Add a unique key and see if it resolves the issue