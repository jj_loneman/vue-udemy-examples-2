# 8 - Communicating Between Components

## 8.2 - Using Props for Parent => Child Communication

App:
![](images/2017-12-01-00-40-04.png)

`src/components/User.vue`

```html
<template>
  <div class="component">
    <h1>The User Component</h1>
    <p>I'm an awesome User!</p>
    <button @click="changeName">Change My Name</button>
    <hr>
    <div class="row">
      <div class="col-xs-12 col-sm-6">
        <app-user-detail :name="name"></app-user-detail>
      </div>
      <div class="col-xs-12 col-sm-6">
        <app-user-edit></app-user-edit>
      </div>
    </div>
  </div>
</template>

<script>
  import UserDetail from './UserDetail.vue';
  import UserEdit from './UserEdit.vue';

  export default {
    data() {
      return {
        name: 'Max'
      }
    },
    methods: {
      changeName() {
        this.name = 'Anna';
      }
    },
    components: {
      appUserDetail: UserDetail,
      appUserEdit: UserEdit
    }
  }
</script>

<style scoped>
  div {
    background-color: lightblue;
  }
</style>
```

* In order to pass `name` data to child component, in `<app-user-detail>`, need to put `:name="name"`
  * `:` same as `v-bind` - binding `name` attribute
    * This makes setting the `name` value dynamic
    * The static way would be doing `name="Max"` or something
  * If you set it equal to `"name"`, you bind it to the `name` property in the `data` function:
    * ![](images/2017-12-01-01-11-01.png)

`src/components/UserDetail.vue`

```html
<template>
  <div class="component">
    <h3>You may view the User Details here</h3>
    <p>Many Details</p>
    <p>User Name: {{ name }}</p>
  </div>
</template>

<script>
  export default {
    props: ['name']
  }
</script>

<style scoped>
  div {
    background-color: lightcoral;
  }
</style>
```

* In order to tell the child property that it will receive data from the outside, we use the `props` property
  * Specify strings that can be settable from outside
  * The string in `props` must match the data value being used in the template:
    * ![](images/2017-12-01-01-07-43.png)
    * This is b/c behind the scenes, it will create another property we can use, just like another property you can set in the `data` object, but now implicitly, because it can be passed from outside
    * In this case, `name` must match the `:name` attribute defined in `User.vue`
      * Otherwise, Vue doesn't know what to do with the name attribute set in the `<app-user-detail>` in `User.vue`

`src/App.vue`

```html
<template>
  <div class="container">
    <div class="row">
      <div class="col-xs-12">
        <app-user></app-user>
      </div>
    </div>
  </div>
</template>

<script>
  import User from './components/User.vue';

  export default {
    components: {
      appUser: User
    }
  }
</script>

<style>
  div.component {
    border: 1px solid black;
    padding: 30px;
  }
</style>
```

`src/main.js`

```javascript
import Vue from 'vue'
import App from './App.vue'

new Vue({
  el: '#app',
  render: h => h(App)
});
```

`src/index.html`

```html
<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="utf-8">
  <title>Vue Components</title>
  <link
      rel="stylesheet"
      href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css"
      integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u"
      crossorigin="anonymous"
  >
</head>
<body>
  <div id="app">
  </div>
  <script src="/dist/build.js"></script>
</body>
</html>
```

### Output

* When you click on button, the name changes from "Max" to "Anna"

![](images/2017-12-01-01-11-58.png)

## 8.3 - Naming "props"

* Stick with lowercase naming conventions, especially when naming props, since there are issues with case-sensitive names in some browsers

## 8.4 - Using "props" in the Child Component

`src/components/UserDetail.vue`

```html
<template>
  <div class="component">
    <h3>You may view the User Details here</h3>
    <p>Many Details</p>
    <p>User Name: {{ switchName() }}</p>
  </div>
</template>

<script>
  export default {
    props: ['myName'],
    methods: {
      switchName() {
        return this.myName.split("").reverse().join("");
      }
    }
  }
</script>

<style scoped>
  div {
    background-color: lightcoral;
  }
</style>
```

* Added a `switchName()` method
* You can use the `props` defined here (`myName`) like a normal object property (`this.myName`) set up in the `data` object

### Output

![](images/2017-12-03-15-17-29.png)

* Clicking the button results in the `User Name: annA` being outputted

## 8.5 - Validating "props"

`src/components/UserDetail.vue`

```html
<template>
  <div class="component">
    <h3>You may view the User Details here</h3>
    <p>Many Details</p>
    <p>User Name: {{ switchName() }}</p>
  </div>
</template>

<script>
  export default {
    props: ['myName'],
    methods: {
      switchName() {
        return this.myName.split("").reverse().join("");
      }
    }
  }
</script>

<style scoped>
  div {
    background-color: lightcoral;
  }
</style>
```

* You should validate arguments being passed into `switchName()` since it will break if given anything that isn't a string
* If in `User.vue`, someone passed in an integer `1` in `<app-user-detail :myName="1"></app-user-detail>`, you'll get the following error:
  * ![](images/2017-12-03-15-49-57.png)
  * Since you can't use `split()` on a number

In `UserDetail.vue`, in this code:

```html
<script>
  export default {
    props: {
      myName: String // or
      myName: [String, Array] // can validate multiple types
    }
  }
  ...
```

* If we want to validate `props`, it should be an object where you give the name of the attribute and the type
* If we pass `1` again to `myName`, we get the following validation error:
  * ![](images/2017-12-03-15-57-38.png)

```html
<script>
  export default {
    props: {
      myName: {
        type: String,
        default: 'Max', // a default value
        required: true // or required (shouldn't have both required and default though
                       // since required: true means default would never happen)
      }
    }
  }
  ...
```

* You can also make `myName` an object and set the type and whether it's `required` or give it a `default` value

```html
<script>
  export default {
    props: {
      myName: {
        type: Object,
        default: function() {
          return {
            name: 'Max'
          }
        }
      }
    }
  }
  ...
```

* If you make `myName` of type `Object` or `Array`, then the `default` should be a function that returns an object

## 8.6 - Using Custom Events for Child => Parent Communication

* We may want to execute a function or pass an event from the child to the parent component so the parent component gets informed of something or gets a new value
  * ![](images/2017-12-03-16-08-15.png)

`src/components/UserDetail.vue`

```html
<template>
  <div class="component">
    <h3>You may view the User Details here</h3>
    <p>Many Details</p>
    <p>User Name: {{ switchName() }}</p>
    <button @click="resetName">Reset Name</button>
  </div>
</template>

<script>
  export default {
    props: {
      myName: {
        type: String
      }
    },
    methods: {
      switchName() {
        return this.myName.split("").reverse().join("");
      },
      resetName() {
        this.myName = 'Max';
      }
    }
  }
</script>

<style scoped>
  div {
    background-color: lightcoral;
  }
</style>
```

* We added a `<button @click="resetName">` element with a `resetName()` method
* If `myName` weren't a primitive (not an Object or Array)
  * Object and Arrays are reference types, so they only exist in memory once
  * This means each object just contains a pointer to the actual object

```html
<script>
  ...
      resetName() {
        this.myName = 'Max';
        this.$emit('nameWasReset', this.myName);
      }
  ...
```

* `$emit()` is a builtin function that emits an event from the child to parent component
  * Give it the name of the event and the property

`src/components/User.vue`

```html
<app-user-detail :myName="name" @nameWasReset="name = $event"></app-user-detail>
```

* Where `name` is the name of the property stored in the `User` component
  * ![](images/2017-12-03-16-17-12.png)
* `$event` refers to the data that was passed in `$emit()`

### Output

![](images/2017-12-03-16-18-29.png)

* When `Change my Name` is clicked, the "User Name" is changed back to "annA"

## 8.7 - Understanding Unirectional Data Flow

* We can't pass data from a child to another child component
  * ![](images/2017-12-03-16-54-02.png)
* Instead, must take the route that parent gives us some method we can execute
  * In the child, when we execute the method provided by the parent (provided as a prop), this will run in the parent (alternatively could use a custom event to which the parent would listen to)
  * Then the parent will pass the updated data to the other child
    * ![](images/2017-12-03-16-55-45.png) 

## 8.8 - Communicating with Callback Functions

`src/components/User.vue`

```html
        <app-user-detail
            :myName="name"
            @nameWasReset="name = $event"
            :resetFn="resetName"
        ></app-user-detail>
```

* Added `resetFn` with the name (the pointer) to the `resetName` function

`src/components/UserDetail.vue`

```html
    <button @click="resetName">Reset Name</button>
    <button @click="resetFn()">Reset Name</button>
...
<script>
  export default {
    props: {
      myName: {
        type: String
      },
      resetFn: Function
    },
...
```

* Duplicated `<button>` element but passed in `resetFn()` instead:
  * ![](images/2017-12-03-17-17-43.png)
  * Here, `resetFn()` was passed down as a prop
  * This is setup to avoid working with a custom event (there's nothing wrong with this, it's just another way)
* Output:
  * ![](images/2017-12-03-17-26-14.png)
  * When clicking the second reset button, it will also reset the name but without using a custom event
* Two ways to pass stuff:
  * `props` with custom event, or
  * `props` with passing callback as a prop which actually executes a method in the parent component, but by passing the parent as a prop, which makes the executable initiate from the child

## 8.9 - Communication between Sibling Components

* Now, want to pass data from one child component to another:
  * ![](images/2017-12-03-17-36-14.png)

`src/components/UserEdit.vue`

```html
<template>
  <div class="component">
    <h3>You may edit the User here</h3>
    <p>Edit me!</p>
    <button @click="editAge">Edit Age</button>
  </div>
</template>

<script>
  export default {
    props: ['userAge'],
    methods: {
      editAge() {
        this.age = 30;
      }
    }
  }
</script>
```

* Added an `editAge()` method and also bound it to the `<button>` element

`src/components/User.vue`

```html
    <div class="row">
      <div class="col-xs-12 col-sm-6">
        <app-user-detail
            :myName="name"
            @nameWasReset="name = $event"
            :resetFn="resetName"
            :userAge="age"
        ></app-user-detail>
      </div>
      <div class="col-xs-12 col-sm-6">
        <app-user-edit :userAge="age"></app-user-edit>
      </div>
    </div>
...
<script>
  import UserDetail from './UserDetail.vue';
  import UserEdit from './UserEdit.vue';

  export default {
    data() {
      return {
        name: 'Max',
        age: 27
      }
    },
...
```

* Added `userAge` attribute and set it to `age` prop

`src/components/UserDetail.vue`

```html
    <p>User Name: {{ switchName() }}</p>
    <p>User Age: {{ userAge }}</p>
...
<script>
  export default {
    props: {
      myName: {
        type: String
      },
      resetFn: Function,
      userAge: Number
    },
...
```

* Output:
  * ![](images/2017-12-03-18-55-28.png)
  * User Age says '27'

`src/components/UserEdit.vue`

```html
    <p>User Age: {{ userAge }}</p>
...
<script>
  export default {
    props: ['userAge'],
    methods: {
      editAge() {
        this.userAge = 30;
      }
    }
  }
</script>
```

* Also added output to `userAge` here in the HTML
* Also changed `this.age` to `this.userAge` to make it the same as the prop name
* Output:
  * ![](images/2017-12-03-18-57-19.png)
  * Clicking 'Edit Age' changes the age from 25 to 30 in the `UserEdit` component but not the `UserDetail` component

`src/components/UserEdit.vue`

```html
<script>
  export default {
    props: ['userAge'],
    methods: {
      editAge() {
        this.userAge = 30;
        this.$emit('ageWasEdited', this.userAge)
      }
    }
  }
</script>
```

* In order to make the change pass to the sibling, still have to pass it through the parent
* Add the `$emit()` function call

`src/components/User.vue`

```html
        <app-user-edit
            :userAge="age"
            @ageWasEdited="age = $event"
        ></app-user-edit>
```

* Added `@ageWasEdited` here to get the age from the event
* Output:
  * ![](images/2017-12-03-19-01-06.png)
  * Clicking 'Edit Age' Also edits the age in the `UserDetails` component
* Clicking the edit button sents the event to the parent which sends it back to both children:
  * ![](images/2017-12-03-19-03-03.png)

## 8.10 - Using an Event Bus for Communication

* This method uses a central class or object to pass data
  * If you come from Angular 2, this is known as a service
    * A service is a central part of the app which you inject in places where you need it which holds tasks or methods you want to use in the whole app

`src/main.js`

```javascript
import Vue from 'vue'
import App from './App.vue'

export const eventBus = new Vue();

new Vue({
  el: '#app',
  render: h => h(App)
});
```

* Here, declare a new const `eventBus`
  * Don't need to put methods in the `Vue()` instance b/c methods needed are already shipped by Vue
* Must create `eventBus` before loading other components, otherwise we won't have access to them, so order is important

`src/components/UserEdit.vue`

```html
<script>
  import { eventBus } from "../main";

  export default {
    props: ['userAge'],
    methods: {
      editAge() {
        this.userAge = 30;
        // this.$emit('ageWasEdited', this.userAge)
        eventBus.$emit('ageWasEdited', this.userAge)
      }
    }
  }
</script>
```

* Need to import `eventBus` from `main.js`
* You can call `eventBus.$emit()`, since `eventBus` is just a Vue instance, so it'll have an `$emit()` method
  * Pass in same data as `this.$emit()` above
  * Difference is that instead of emitting data from `this` instance, it's emitted on the `eventBus` instance

`src/components/UserDetail.vue`

```html
<script>
  import { eventBus } from "../main";
...
    methods: {
      switchName() {
        return this.myName.split("").reverse().join("");
      },
      resetName() {
        this.myName = 'Max';
        this.$emit('nameWasReset', this.myName);
      },
      created() {
        eventBus.$on('ageWasEdited', (age) => {
          this.userAge = age;
        });
      }
    }
...
```

* The `eventBus` is a separate instance, can go to `UserDetail` component and create a new lifecycle hook: the `created()` hook
  * Use the hook b/c want to set up a listener to the event
  * The listener should keep running from the beginning of the component
  * When the component is created, you want to register the listener then
* Add `eventBus.$on();` to hook
  * In this method, the data is always used in the second argument which is always a callback, which should get executed whenever an event occurs
  * The callback automatically gets executed by Vue, which always gets the data passed with the event passed as an argument
    * The argument can be named whatever, `data` or `age`

`src/components/User.vue`

```html
    <p>Age is {{ age }}</p>
```

* Output:
  * ![](images/2017-12-03-19-28-19.png)
* If you output the age in the `User` component, then when you click 'Edit Age' in the `UserEdit` component, it passes it to the `UserDetails` component without going through the `User` component
* In small to medium sized applications, this is a fine approach, although with larger apps, probably want to use state mangement through Vuex

## 8.11 - Centralizing Code in an Event Bus

`src/main.js`

```javascript
export const eventBus = new Vue({
  methods: {
    changeAge(age) {
      this.$emit('ageWasEdited', age);
    }
  }
});
```

* Added `methods` object with `changeAge()`
* Makes sense to put the method here so you don't have duplicate code and just want to store logic centrally
* Don't have to have `$emit()` events here, could have any code that's accessible as long as you import it and access the methods you provide on the bus
  * Same goes for any `data` properties created there

`src/components/UserEdit.vue`

```html
<script>
  import { eventBus } from "../main";

  export default {
    props: ['userAge'],
    methods: {
      editAge() {
        this.userAge = 30;
        // this.$emit('ageWasEdited', this.userAge)
        // eventBus.$emit('ageWasEdited', this.userAge)
        eventBus.changeAge(this.userAge);
      }
    }
  }
</script>
```

* You can call the `changeAge()` function from the `eventBus` object
  * Clicking the 'Edit Age' button still works the same as before:
    * ![](images/2017-12-03-19-44-18.png)