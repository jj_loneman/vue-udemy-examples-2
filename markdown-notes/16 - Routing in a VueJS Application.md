# 16 - Routing in a VueJS Application

## 16.1 - Introduction

* A single page app (SPA) technically only has one page (index.html)
* We re-render the page so it looks different
* So the entire app is handled by Vue

## 16.2 - Setting up the VueJS Router (vue-router)

```commandline
npm install --save vue-router
```

`src/routes.js`

```javascript
import User from './components/user/User';

export const routes = [
  {
    path: '',
    component: Home
  },
  {
    path: '/user',
    component: User
  },
];
```

`src/main.js`

```javascript
import Vue from 'vue'
import VueRouter from 'vue-router';
import App from './App.vue'
import { routes } from './routes';

Vue.use(VueRouter);

const router = new VueRouter({
  routes
});

new Vue({
  el: '#app',
  router,
  render: h => h(App)
});
```

* The `routes` passed into `VueRouter` is the ES6 shorthand for `routes: routes`
* The `router` passed into `new Vue()` is the ES6 shorthand for `router: router`

`src/App.vue`

```html
<template>
  <div class="container">
    <div class="row">
      <div class="col-xs-12 col-sm-8 col-sm-offset-2 col-md-6 col-md-offset-3">
        <h1>Routing</h1>
        <hr>
        <router-view></router-view>
      </div>
    </div>
  </div>
</template>
```

* `<router-view>`
  * Builtin component that tells the router to load the necessary component in that place

* When navigating to root path:
  * ![](images/2017-12-05-05-02-22.png)
* Going to `/user`:
  * ![](images/2017-12-05-05-03-03.png)

## 16.3 - Understanding Routing Modes (Hash vs History)

* If you go to a URL like `/` (no hash), the request gets sent to the server
* If you go to `localhost:8080/#/user`:
  * The part before the `#` (`localhost:8080`) gets sent to the server and retrieves `index.html`
  * The part after the `#` (`/user`) is handed over to the running JS app and handled by the app
* You always need to return the `index.html` file
  * Even in cases of 404 error

`src/main.js`

```javascript
const router = new VueRouter({
  routes,
  mode: 'history'
});
```

* If you don't want to use the `#` mode in the URL, add `mode: 'history'` (`mode: 'hash'` is the default)

## 16.4 - Navigating with Router Links

`src/Header.vue`

```html
<template>
  <ul class="nav nav-pills">
    <li role="presentation">
      <router-link to="/">Home</router-link>
    </li>
    <li role="presentation">
      <router-link to="/user">User</router-link>
    </li>
  </ul>
</template>

<script>
  export default {
    name: "header"
  }
</script>
```

* Don't want to use `<a href="#">`, because although it would actually work fine, the link sends a request to the server and you don't want to handle it that way
* The `<router-link>` tag ends up creating an `<a>` tag in the end, but it allows you to set it up differently than the normal anchor tag
  * It doesn't do the default behavior of sending the request, so it doesn't refresh the page, it instead listens to the click and takes you to the route

`src/App.vue`

```html
<template>
  <div class="container">
    <div class="row">
      <div class="col-xs-12 col-sm-8 col-sm-offset-2 col-md-6 col-md-offset-3">
        <h1>Routing</h1>
        <hr>
        <app-header></app-header>
        <router-view></router-view>
      </div>
    </div>
  </div>
</template>

<script>
  import Header from './components/Header';
  export default {
    components: {
      appHeader: Header
    }
  }
</script>
```

* When "Home" is clicked:
  * ![](images/2017-12-05-05-21-06.png)
* When "User" is clicked:
  * ![](images/2017-12-05-05-21-24.png)
* The code behind the scenes:
  * ![](images/2017-12-05-05-22-10.png)
  * Generates an anchor tag and sets the class to `router-link-active` if it's active

## 16.5 - Where am I? - Styling Active Links

* The problem is that bootstrap's class is called `active` while `vue-router`'s is called `router-link-active`
* We can remove the `<li>` surround tags and have this instead:

`src/Header.vue`

```html
<template>
  <ul class="nav nav-pills">
    <router-link
        to="/"
        tag="li"
        active-class="active"
        exact
    >
      <a>Home</a>
    </router-link>
    <router-link
        to="/user"
        tag="li"
        active-class="active"
    >
      <a>User</a>
    </router-link>
  </ul>
</template>
```

* Override what the `<router-link>` should view using `tag="li"`
* `<router-link>` will set up a router link on the `<li>`
  * It will take any content inside it and set it up as a link
  * The real link is set up by the router link
* `active-class="active"` will set the active class to `active` instead of `router-link-active`
* Default behavior
  * The default is to see if the current link starts with the path in `to`
  * This leads to behavior like this:
    * ![](images/2017-12-05-05-32-38.png)
    * Where `/user` starts with `/`, so "Home" is marked as active
* `exact`
  * This overrides the default behavior
  * The route must be an exact match
  * Now leads to expected output:
    * ![](images/2017-12-05-05-33-55.png)

## 16.6 - Navigating from Code (Imperative Naviation)

`src/components/User.vue`

```html
<template>
  <div>
    <h1>The User Page</h1>
    <hr>
    <button
        @click="navigateToHome"
        class="btn btn-primary"
    >
      Go to Home
    </button>
  </div>
</template>

<script>
  export default {
    methods: {
      navigateToHome() {
        this.$router.push('/');
      }
    }
  }
</script>
```

* `this.$router.push()` pushes the route to the stack of existing routes
  * Takes the path where you want to go `'/'`
  * Can also pass in an object `{ path: '/' }`
* When click "User":
  * ![](images/2017-12-05-05-38-24.png)
* When click "Go to Home"
  * ![](images/2017-12-05-05-38-40.png)

## 16.7 - Setting Up Route Parameters

`src/routes.js`

```javascript
import Home from './components/Home';
import User from './components/user/User';

export const routes = [
  {
    path: '',
    component: Home
  },
  {
    path: '/user/:id',
    component: User
  },
];
```

* Here, added `:id` to `/user` path, which dynamically takes an `id`

## 16.8 - Fetching and Using Route Parameters

`src/components/User.vue`

```html
<template>
  <div>
    <h1>The User Page</h1>
    <hr>
    <p>Loaded ID: {{ id }}</p>
    <button
        @click="navigateToHome"
        class="btn btn-primary"
    >
      Go to Home
    </button>
  </div>
</template>

<script>
  export default {
    data() {
      return {
        id: this.$route.params.id
      }
    },
    methods: {
      navigateToHome() {
        this.$router.push('/');
      }
    }
  }
</script>
```

* `this.$route` is the active route that was loaded
  * `this.$route.params` holds an object of key-value pairs of params
* `/user/10`:
  * ![](images/2017-12-05-05-45-34.png)
* `/user/5`:
  * ![](images/2017-12-05-05-45-51.png)

## 16.9 - Reacting to Changes in Route Parameters

`src/components/User.vue`

```html
<script>
...
    watch: {
      '$route'(to, from) {
        this.id = to.params.id;
      }
    },
...
```

* In the `User` component, the `id` is loaded once when the component loads, but doesn't change if the subpath is different (`/user/1` => `user/2`)
* We can set up a watcher so when `$route` changes
* `to` is a route object
* When click on "User 2", the Loaded ID changes with the watcher in place:
  * ![](images/2017-12-05-05-51-58.png)

## 16.10 - vue-router 2.2: Extract Route Params via "props"

* As of vue-router v2.2, you can also bind your route params to props of the target components
* This eliminates the need of watching the route
* Three ways of doing this:
  * Pass static value
  * Bind dynamic value to props
  * Or use function ot convert your dynamic value
* Official example: <https://github.com/vuejs/vue-router/tree/dev/examples/route-props>

## 16.11 - Setting Up Child Routes (Nested Routes)

`src/routes.js`

```javascript
import Home from './components/Home';
import User from './components/user/User';
import UserDetail from './components/user/UserDetail'
import UserEdit from './components/user/UserEdit'
import UserStart from './components/user/UserStart'

export const routes = [
  { path: '', component: Home },
  { path: '/user', component: User, children: [
      { path: '', component: UserStart },
      { path: ':id', component: UserDetail },
      { path: ':id/edit', component: UserEdit },
    ]
  },
];
```

* Here, the `/` will be appended after `/user`

`src/components/User.vue`

```html
<template>
  <div>
    <h1>The User Page</h1>
    <hr>
    <button
        @click="navigateToHome"
        class="btn btn-primary"
    >
      Go to Home
    </button>
    <router-view></router-view>
  </div>
</template>

<script>
  export default {
    methods: {
      navigateToHome() {
        this.$router.push('/');
      }
    }
  }
</script>
```

* The nested routes won't be loaded in `<router-view>` in `App` component, because that's the root router
  * Need to add it in `User.vue` to load the subroutes
* Output:
  * ![](images/2017-12-05-06-02-44.png)
* Going to `/user/1`:
  * ![](images/2017-12-05-06-03-10.png)

## 16.12 - Navigating to Nested Routes

`src/components/UserStart.vue`

```html
<template>
  <div>
    <p>Please select a User</p>
    <hr>
    <ul class="list-group">
      <router-link
          tag="li"
          to="/user/1"
          class="list-group-item"
          style="cursor: pointer"
      >User 1
      </router-link>
      <router-link
          tag="li"
          to="/user/2"
          class="list-group-item"
          style="cursor: pointer"
      >User 2
      </router-link>
      <router-link
          tag="li"
          to="/user/3"
          class="list-group-item"
          style="cursor: pointer"
      >User 3
      </router-link>
    </ul>
  </div>
</template>
```

`src/components/UserDetail.vue`

```html
<template>
  <div>
    <h3>Some User Details</h3>
    <p>User loaded has ID: {{ $route.params.id }}</p>
  </div>
</template>
```

* Don't need to watch changes b/c if on the `UserDetail` component, no way to go to user 2 - the component will get recreated anyway

* Click on "User 3" takes you to the `/user/3` path
  * ![](images/2017-12-05-06-07-43.png)
  * ![](images/2017-12-05-06-08-57.png)

## 16.13 - Making Router Links more Dynamic

`src/components/UserDetail.vue`

```html
<template>
  <div>
    <h3>Some User Details</h3>
    <p>User loaded has ID: {{ $route.params.id }}</p>
    <router-link
        tag="button"
        :to="'/user/' + $route.params.id + '/edit'"
    >
      Edit User
    </router-link>
  </div>
</template>
```

* Output:
  * ![](images/2017-12-05-06-13-06.png)
  * ![](images/2017-12-05-06-13-18.png)

## 16.14 - A Better Way of Creating Links - With Named Routes

`src/routes.js`

```javascript
export const routes = [
  { path: '', component: Home },
  { path: '/user', component: User, children: [
      { path: '', component: UserStart },
      { path: ':id', component: UserDetail },
      { path: ':id/edit', component: UserEdit, name: 'userEdit' },
    ]
  },
];
```

* Here, we named the `:id/edit` route `userEdit`

`src/components/UserDetail.vue`

```html
    <router-link
        tag="button"
        :to="{ name: 'userEdit', params: { id: $route.params.id } }"
        class="btn btn-primary"
    >
```

* You can pass in object to `:to` passing in dynamic `id`

`src/routes.js`

```javascript
export const routes = [
  { path: '', component: Home, name: 'home' },
```

* You can do the same thing with `home`

`src/components/User.vue`

```html
<script>
  export default {
    methods: {
      navigateToHome() {
        this.$router.push({ name: 'home' });
      }
    }
  }
</script>
```

* Here, you can navigate to named route as well

## 16.15 - Using Query Parameters

`src/components/UserDetail.vue`

```html
    <router-link
        tag="button"
        :to="{ name: 'userEdit', params: { id: $route.params.id }, query: { locale: 'en', q: '100' } }"
        class="btn btn-primary"
    >
```

* By using the `query` object, we can pass in request params to the url:
  * ![](images/2017-12-05-06-22-39.png)

`src/components/UserEdit.vue`

```html
<template>
  <div>
    <h3>Edit the User</h3>
    <p>Locale: {{ $route.query.locale }}</p>
    <p>Analytics: {{ $route.query.q }}</p>
  </div>
</template>
```

* Changing `q` to `q=105` outputs this data:
  * ![](images/2017-12-05-06-24-18.png)

## 16.16 - Multiple Router Views (Named Router Views)

`src/routes.js`

```javascript
export const routes = [
  { path: '', component: Home, name: 'home', components: {
      default: Home,
      'header-top': Header
    } },
  { path: '/user', components: {
      default: User,
      'header-bottom': Header
    }, children: [
      { path: '', component: UserStart },
      { path: ':id', component: UserDetail },
      { path: ':id/edit', component: UserEdit, name: 'userEdit' },
    ]
  },
];
```

* We named the `components` of the router views

`src/App.vue`

```html
<template>
  <div class="container">
    <div class="row">
      <div class="col-xs-12 col-sm-8 col-sm-offset-2 col-md-6 col-md-offset-3">
        <h1>Routing</h1>
        <hr>
        <router-view name="header-top"></router-view>
        <router-view></router-view>
        <router-view name="header-bottom"></router-view>
      </div>
    </div>
  </div>
</template>
```

* Having the named routes makes it easy to reserve that spot of the code to dynamically render pieces of the app there
* Default: header is loaded at the top (the buttons)
  * ![](images/2017-12-05-06-31-25.png)
* In `User` component, header (the buttons) are loaded at the bottom:
  * ![](images/2017-12-05-06-32-06.png)

## 16.17 - Redirecting

`src/routes.js`

```javascript
  { path: '/redirect-me', redirect: '/' }
  // or can use the named router view
  { path: '/redirect-me', redirect: { name: 'home' } }
```

* Going to `/redirect-me` redirects you to `/`
* Or you can use the named router view way

## 16.18 - Setting Up "Catch All" Routes / Wildcards

`src/routes.js`

```javascript
  { path: '*', redirect: '/' }
```

* Redirects anything not specified to the home page

## 16.19 - Animating Route Transitions

`src/App.vue`

```html
        <transition name="slide" mode="out-in">
          <router-view></router-view>
        </transition>
```

* This enables a slide transition whenever you load a different component

## 16.20 - Passing the Hash Fragment

`src/components/user/UserEdit.vue`

```html
<template>
  <div>
    <h3>Edit the User</h3>
    <p>Locale: {{ $route.query.locale }}</p>
    <p>Analytics: {{ $route.query.q }}</p>
    <div style="height: 700px"></div>
    <p id="data">Some extra data</p>
  </div>
</template>
```

* Going to `#data` at the end: the browser will automatically jump to that tag:
  * ![](images/2017-12-05-06-47-11.png)
  * ![](images/2017-12-05-06-47-23.png)

`src/components/user/UserDetail.vue`

```html
<template>
  <div>
    <h3>Some User Details</h3>
    <p>User loaded has ID: {{ $route.params.id }}</p>
    <router-link
        tag="button"
        :to="link"
        class="btn btn-primary"
    >
      Edit User
    </router-link>
  </div>
</template>

<script>
  export default {
    data() {
      return {
        link: {
          name: 'userEdit',
          params: {
            id: this.$route.params.id
          },
          query: {
            locale: 'en',
            q: '100'
          },
          hash: '#data'
        }
      }
    }
  }
</script>
```

* Here, we pass `#data`, but the browser doesn't jump to the element

## 16.21 - Controlling the Scrolling Behavior

`src/main.js`

```javascript
const router = new VueRouter({
  routes,
  mode: 'history',
  scrollBehavior(to, from, savedPosition) {
    if (savedPosition) {
      return savedPosition;
    }
    if (to.hash) {
      return {
        selector: to.hash
      };
    }
    return {
      x: 0,
      y: 0
    }
  }
});
```

* `scrollBehavior()`
  * Expects to get back either an object of an x and y coordinate or a selector
* `to.hash` in the `if` statement just sees if a hash is present
* `savedPosition` is the last position before the user scrolled
  * So when the user clicks the back button, it takes them back to the last position
* This code will take the user to the selected if it's passed in by the hash
  * If no hash is defined, they are scrolled to the top

## 16.22 - Protecting Routes with Guards

* You want to control if a user is allowed to access a certain route
  * Either entering or leaving it

## 16.23 - Using the "beforeEnter" Guard

* Three ways to set up the "is the user allowed to enter" check

### main.js

```javascript
router.beforeEach((to, from, next) => {
  console.log('global beforeEach');
  next();
});
```

* This gets executed before every routing action
  * May use for only very generic checks
* **You must execute `next()` for it to continue, or it will assume it's not allowed to continue and will exit``**
  * You can pass nothing
    * It will load the desired page
  * You can pass `false`
    * It will abort
  * You can pass a path
* You can see the function gets executed every routing action:
  * ![](images/2017-12-05-07-02-40.png)

### routes.js

```javascript
      { path: ':id', component: UserDetail, beforeEnter: (to, from, next) => {
          console.log('inside route setup');
          next();
        } },
```

* This specifically protects the `UserDetail` route
* This is the next step of granularity, where we check it on the route level

### In the Component

`src/components/user/UserDetail.vue`

```javascript
    beforeRouteEnter(to, from, next) {
      // If user is authenticated, allow access
      if (true) {
        next();
      } else {
        next(false);
      }
    }
```

* If you don't call `next()`, the component doesn't get loaded
* In the `beforeRouteEnter()` method, you're in the file, but the component hasn't been initialized yet, so you can't reference any data properties
  * If you need to access it, you can pass a callback in the `next()` method:
    ```javascript
      next(vm => {
        vm.link;
      });
    ```
    but, at this time, the component is created
  * Instead, you want to have some check before the component like:
    ```javascript
    if (user_is_authenticated) {
      next();
    } else {
      next(false);
    }
    ```

## 16.24 - Using the "beforeLeave" Guard

`src/components/user/UserEdit.vue`

```html
<template>
  <div>
    <h3>Edit the User</h3>
    <p>Locale: {{ $route.query.locale }}</p>
    <p>Analytics: {{ $route.query.q }}</p>
    <hr>
    <button
        class="btn btn-primary"
        @click="confirmed = true"
    >
      Confirm
    </button>
    <div style="height: 700px"></div>
    <p id="data">Some extra data</p>
  </div>
</template>

<script>
  export default {
    data() {
      return {
        confirmed: false
      }
    },
    beforeRouteLeave(to, from, next) {
      if (this.confirmed) {
        next();
      } else {
        if (confirm('Are you sure?')) {
          next();
        } else {
          next(false);
        }
      }
    }
  }
</script>
```

* Checking route leave on a global level is too late since the navigation would already be on its way
* If you click "Cancel" on the Alert, you'll stay on the page:
  * ![](images/2017-12-05-07-16-53.png)
* If you click "OK", you'll leave the page

## 16.25 - Loading Routes Lazily

* With lazy loading, you only load parts of the app when you need it
* Webpack will not load the imports in the initial bundle, but will create several other bundles

`src/routes.js`

```javascript
const User = resolve => {
  require.ensure(['./components/user/User'], () => {
    resolve(require('./components/user/User'));
  });
};
```

* Whenever we want to load something that exists in this place:
  * ![](images/2017-12-05-07-23-54.png)
  * We want to load `User` when we visit the route:
    * ![](images/2017-12-05-07-24-44.png)
  * We execute the `resolve()` function, which is like a promise, which resolves the path you should then really use
* When `User` was clicked on, it loaded 2 new bundles:
  * ![](images/2017-12-05-07-29-30.png)

`src/routes.js`

```javascript
const User = resolve => {
  require.ensure(['./components/user/User'], () => {
    resolve(require('./components/user/User'));
  }, 'user');
};
const UserStart = resolve => {
  require.ensure(['./components/user/UserStart'], () => {
    resolve(require('./components/user/UserStart'));
  }, 'user');
};
```

* Can also group loading like so

## 16.26 - Resources and Links

* vue-router Github: <https://github.com/vuejs/vue-router>
* vue-router docs: <https://router.vuejs.org/en/>